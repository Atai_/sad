module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    ['@babel/plugin-proposal-decorators', {legacy: true}],
    [
      'module-resolver',
      {
        extensions: [
          '.ts',
          '.json',
          '.tsx',
          '.js',
          '.jsx',
          '.png',
          '.jpg',
          '.jpeg',
        ],
        alias: {
          '@styles': './app/styles',
          '@screens': './app/screens',
          '@navigation': './app/navigation',
          '@config': './app/config',
          '@utils': './app/utils',
          '@components': './app/components',
          '@store': './app/store',
          '@assets': './app/assets',
          '@i18n': './app/i18n',
          '@hooks': './app/hooks',
          '@customTypes': './app/types',
          '@constants': './app/constants',
          '@services': './app/services',
        },
      },
    ],
    'react-native-reanimated/plugin',
  ],
};
