import React from 'react';

import {
  StackNavigationOptions,
  CardStyleInterpolators,
} from '@react-navigation/stack';
import {AppColors, Spacing} from '@styles/';

export const cardStyleInterpolators: StackNavigationOptions = {
  cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
};
export const HEADER_NONE: StackNavigationOptions = {
  headerShown: false,
  ...cardStyleInterpolators,
};

export const HEADER_APP: StackNavigationOptions = {
  headerStyle: {
    shadowRadius: 0,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0,
    elevation: 0,
    backgroundColor: AppColors.WHITE,
  },
  headerLeftContainerStyle: {
    marginLeft: Spacing.SCALE_16,
  },

  headerShown: true,
  headerTruncatedBackTitle: '',
  // headerTitleAlign: 'center',
  ...cardStyleInterpolators,
};
