import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {NAVIGATION_BOTTOM_MENU, NAVIGATION_HOME} from './screenNames';
import {HEADER_NONE} from './screenOptions';
import {BottomTabNavigator} from './BottomTabNavigator';
import {HomeServiceCreateScreen} from '@screens/home/screens/HomeService/HomeServiceCreateScreen';
import {SelectAddressScreen} from '@screens/home/screens/SelectAddressScreen';

const Stack = createStackNavigator();

export const AppNavigator = () => (
  <Stack.Navigator screenOptions={HEADER_NONE}>
    <Stack.Screen
      name={NAVIGATION_BOTTOM_MENU.MAIN}
      component={BottomTabNavigator}
    />
    <Stack.Screen
      component={HomeServiceCreateScreen}
      name={NAVIGATION_HOME.CREATE_DELIVERY}
      options={{...HEADER_NONE}}
    />
    <Stack.Screen
      component={SelectAddressScreen}
      name={NAVIGATION_HOME.SELECT_ADDRESS}
      options={{...HEADER_NONE}}
    />
  </Stack.Navigator>
);
