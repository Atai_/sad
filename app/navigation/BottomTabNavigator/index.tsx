import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {NAVIGATION_BOTTOM_MENU} from '../screenNames';
import {HomeStack} from './Stacks/HomeStack';
import {BOTTOM_MENU_TITLE} from '@navigation/screenTitles';
import {AppTabBar} from '@components/AppTabBar';
import {MessagesStack} from './Stacks/MessagesStack';
import {OrdersStack} from './Stacks/OrdersStack';
import {ProfileStack} from './Stacks/ProfileStack';

const Tabs = createBottomTabNavigator();

export const BottomTabNavigator = () => {
  return (
    <Tabs.Navigator
      initialRouteName={NAVIGATION_BOTTOM_MENU.HOME}
      tabBar={(props) => <AppTabBar {...props} />}>
      <Tabs.Screen
        name={NAVIGATION_BOTTOM_MENU.HOME}
        component={HomeStack}
        options={{
          title: BOTTOM_MENU_TITLE.home,
        }}
      />
      <Tabs.Screen
        name={NAVIGATION_BOTTOM_MENU.MESSAGES}
        component={MessagesStack}
        options={{
          title: BOTTOM_MENU_TITLE.messages,
        }}
      />

      <Tabs.Screen
        name={NAVIGATION_BOTTOM_MENU.ORDERS}
        component={OrdersStack}
        options={{
          title: BOTTOM_MENU_TITLE.orders,
        }}
      />
      <Tabs.Screen
        name={NAVIGATION_BOTTOM_MENU.PROFILE}
        component={ProfileStack}
        options={{
          title: BOTTOM_MENU_TITLE.profile,
        }}
      />
    </Tabs.Navigator>
  );
};
