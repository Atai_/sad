import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {HEADER_NONE} from '@navigation/screenOptions';
import {NAVIGATION_MESSAGES} from '@navigation/screenNames';
import {HomeScreen} from '@screens/home/HomeScreen';

const Messages = createStackNavigator();

export const MessagesStack = () => (
  <Messages.Navigator screenOptions={HEADER_NONE}>
    <Messages.Screen name={NAVIGATION_MESSAGES.MAIN} component={HomeScreen} />
  </Messages.Navigator>
);
