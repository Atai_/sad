import React from 'react';

import {createStackNavigator} from '@react-navigation/stack';
import {HEADER_NONE} from '@navigation/screenOptions';
import {NAVIGATION_ORDERS} from '@navigation/screenNames';
import {HomeScreen} from '@screens/home/HomeScreen';

const Orders = createStackNavigator();

export const OrdersStack = () => (
  <Orders.Navigator screenOptions={HEADER_NONE}>
    <Orders.Screen component={HomeScreen} name={NAVIGATION_ORDERS.MAIN} />
  </Orders.Navigator>
);
