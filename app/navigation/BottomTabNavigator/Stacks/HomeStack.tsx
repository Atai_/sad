import React from 'react';

import {createStackNavigator} from '@react-navigation/stack';
import {NAVIGATION_HOME} from '@navigation/screenNames';
import {HEADER_NONE} from '@navigation/screenOptions';
import {HomeScreen} from '@screens/home/HomeScreen';

const Home = createStackNavigator();

export const HomeStack = () => (
  <Home.Navigator screenOptions={HEADER_NONE}>
    <Home.Screen component={HomeScreen} name={NAVIGATION_HOME.MAIN} />
  </Home.Navigator>
);
