import React from 'react';

import {createStackNavigator} from '@react-navigation/stack';
import {HEADER_NONE} from '@navigation/screenOptions';
import {NAVIGATION_PROFILE} from '@navigation/screenNames';
import {HomeScreen} from '@screens/home/HomeScreen';

const Profile = createStackNavigator();

export const ProfileStack = () => (
  <Profile.Navigator screenOptions={HEADER_NONE}>
    <Profile.Screen component={HomeScreen} name={NAVIGATION_PROFILE.MY_DATA} />
  </Profile.Navigator>
);
