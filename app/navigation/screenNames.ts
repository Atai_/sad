export enum NAVIGATION_BOTTOM_MENU {
  MAIN = 'NAVIGATION/BOTTOM/MENU/MAIN',
  MESSAGES = 'NAVIGATION/BOTTOM/MENU/MESSAGES',
  SEARCH = 'NAVIGATION/BOTTOM/MENU/SEARCH',
  HOME = 'NAVIGATION/BOTTOM/MENU/HOME',
  ORDERS = 'NAVIGATION/BOTTOM/MENU/ORDERS',
  PROFILE = 'NAVIGATION/BOTTOM/MENU/PROFILE',
}

export enum NAVIGATION_HOME {
  MAIN = 'NAVIGATION/BOTTOM/MENU/HOME/MAIN',
  CREATE_DELIVERY = 'NAVIGATION/BOTTOM/MENU/HOME/CREATE_DELIVERY',
  SELECT_ADDRESS = 'NAVIGATION/BOTTOM/MENU/HOME/SELECT_ADDRES',
}

export enum NAVIGATION_MESSAGES {
  MAIN = 'NAVIGATION/BOTTOM/MENU/MESSAGES/MAIN',
}

export enum NAVIGATION_SEARCH {
  MAIN = 'NAVIGATION/BOTTOM/MENU/SEARCH/MAIN',
  CREATE_ORDER = 'NAVIGATION/BOTTOM/MENU/SEARCH/CREATE_ORDER',
}

export enum NAVIGATION_CREATE_ORDER {
  MAIN = 'NAVIGATION/SEARCH/CREATE_ORDER/MAIN',
  CREATE_ORDER = 'NAVIGATION/SEARCH/CREATE_ORDER/CREATE_ORDER',
  DETAILS = 'NAVIGATION/SEARCH/CREATE_ORDER/DETAILS',
  DESCRIPTION = 'NAVIGATION/SEARCH/CREATE_ORDER/DESCRIPTION',
}

export enum NAVIGATION_ORDERS {
  MAIN = 'NAVIGATION/BOTTOM/MENU/ORDERS/MAIN',
}

export enum NAVIGATION_PROFILE {
  MAIN = 'NAVIGATION/BOTTOM/MENU/PROFILE/MAIN',
  MY_DATA = 'NAVIGATION/BOTTOM/MENU/PROFILE/MY_DATA',
  ABOUT_SERVICE = 'NAVIGATION/BOTTOM/MENU/PROFILE/ABOUT_SERVICE',
  PAYMENT = 'NAVIGATION/BOTTOM/MENU/PROFILE/PAYMENT',
  MY_ADDRESSES = 'NAVIGATION/BOTTOM/MENU/PROFILE/MY_ADDRESSES',
  PROMOCODE = 'NAVIGATION/BOTTOM/MENU/PROFILE/PROMOCODE',
  FOR_PERFORMERS = 'NAVIGATION/BOTTOM/MENU/PROFILE/FOR_PERFORMERS',
  QA = 'NAVIGATION/BOTTOM/MENU/PROFILE/QA',
}
