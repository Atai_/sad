export const BOTTOM_MENU_TITLE = {
  messages: 'Сообщения',
  search: 'Поиск',
  home: 'Главная',
  orders: 'Заказы',
  profile: 'Профиль',
};
