import React from 'react';

import {createStackNavigator} from '@react-navigation/stack';
import {HEADER_NONE} from '@navigation/screenOptions';

const CreateOrder = createStackNavigator();

export const CreateOrderStack = () => (
  <CreateOrder.Navigator screenOptions={HEADER_NONE}>
    {/* <CreateOrder.Screen
      component={CreateOrderScreen}
      name={NAVIGATION_CREATE_ORDER.CREATE_ORDER}
    />
    <CreateOrder.Screen
      component={CreateOrderDetailsScreen}
      name={NAVIGATION_CREATE_ORDER.DETAILS}
    />
    <CreateOrder.Screen
      component={CreateOrderDescriptionScreen}
      name={NAVIGATION_CREATE_ORDER.DESCRIPTION}
    /> */}
  </CreateOrder.Navigator>
);
