export const MARKER_ICONS = {
  PERFORMER: require('@assets/images/map/Performer_in_move_Icon.png'),
  MY_LOCATION: require('@assets/images/map/My_loc.png'),
  SELECTED_LOCATION: require('@assets/images/map/Location_red_icon_40.png'),
  LOCATION: require('@assets/images/map/Location_red_icon_20.png'),
};
