import {ServiceTypes} from '@customTypes/service';

export const SERVICE_TEXT = {
  [ServiceTypes.SHOPPING_COURIER]: 'Помощник покупок',
  [ServiceTypes.ORDER_COURIER]: 'Доставщик заказов',
  [ServiceTypes.DELIVERY_COURIER]: 'Просто курьер',
};

export const SERVICE_DESCRIPTION = {
  [ServiceTypes.SHOPPING_COURIER]: 'Купить и доставить',
  [ServiceTypes.ORDER_COURIER]: 'Просто доставить',
  [ServiceTypes.DELIVERY_COURIER]: 'Забрать готовый заказ',
};

export const SERVICE_ICON = {
  [ServiceTypes.DELIVERY_COURIER]: require('@assets/images/services/Courier_Icon.png'),
  [ServiceTypes.SHOPPING_COURIER]: require('@assets/images/services/Shopper_Icon.png'),
  [ServiceTypes.ORDER_COURIER]: require('@assets/images/services/PostDelivery_Icon.png'),
};
export const SERVICE_IMAGE = {
  [ServiceTypes.DELIVERY_COURIER]: require('@assets/images/services/Delivery_BG.png'),
  [ServiceTypes.SHOPPING_COURIER]: require('@assets/images/services/Shopper_BG.png'),
  [ServiceTypes.ORDER_COURIER]: require('@assets/images/services/PostBG.png'),
};
