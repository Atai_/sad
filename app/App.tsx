/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import {StatusBar} from 'react-native';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {NavigationContainer} from '@react-navigation/native';
import {AppNavigator} from './navigation/AppNavigator';
import {Provider, observer} from 'mobx-react';
import {BottomSheetModalProvider} from '@gorhom/bottom-sheet';
import store from './store';

// @ts-ignore
declare const global: {HermesInternal: null | {}};

const stores = {
  rootStore: store,
};

const App = observer(() => {
  const [value, setValue] = React.useState('');
  return (
    <Provider {...stores}>
      <BottomSheetModalProvider>
        <StatusBar barStyle="dark-content" />
        <SafeAreaProvider>
          <NavigationContainer>
            <AppNavigator />
          </NavigationContainer>
        </SafeAreaProvider>
      </BottomSheetModalProvider>
    </Provider>
  );
});

export default App;
