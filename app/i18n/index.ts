import I18n from 'i18n-js';

import ru from './translations/ru.json';

I18n.fallbacks = true;
I18n.translations = {
  ru,
};

I18n.defaultLocale = 'ru';

export const translate = I18n.t;
