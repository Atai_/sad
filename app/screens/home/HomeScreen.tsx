import * as React from 'react';
import {ScrollView, StyleSheet, View} from 'react-native';
import commonStyles from '@styles/commonStyles';
import {SafeAreaView} from 'react-native-safe-area-context';
import {StackScreenProps} from '@react-navigation/stack';

import {HomeHeader} from './components/HomeHeader';
import {Spacing} from '@styles/';
import {HomeService} from './components/HomeService';
import {ServiceTypes} from '@customTypes/service';
import {HomeOrders} from './components/HomeOrders';
import {HomeStatus} from './components/HomeStatus/HomeStatus';
import {NAVIGATION_HOME} from '@navigation/screenNames';
import {HomeChats} from './components/HomeChats';

export const HomeScreen: React.FC<StackScreenProps<any>> = ({navigation}) => {
  return (
    <SafeAreaView edges={['top']} style={commonStyles.container}>
      <ScrollView
        style={styles.scroll}
        contentContainerStyle={styles.contentScroll}>
        <HomeStatus />
        <View style={styles.content}>
          <HomeOrders />
          <HomeHeader
            title="Выбери услугу"
            description="Как это работает? Нажми на знак вопроса"
          />
          <HomeChats />
          {(Object.keys(ServiceTypes) as Array<keyof typeof ServiceTypes>).map(
            (key) => {
              return (
                <HomeService
                  key={key}
                  type={ServiceTypes[key]}
                  onPress={() =>
                    navigation.navigate(NAVIGATION_HOME.CREATE_DELIVERY, {
                      type: ServiceTypes[key],
                    })
                  }
                />
              );
            },
          )}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  scroll: {
    flex: 1,
  },
  contentScroll: {
    paddingVertical: Spacing.SCALE_8,
  },
  content: {
    paddingHorizontal: Spacing.SCALE_16,
  },
});
