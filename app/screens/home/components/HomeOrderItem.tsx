import * as React from 'react';
import {View, Text, Image, StyleSheet, Platform} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import {ServiceTypes, ServiceTypesLiteral} from '@customTypes/service';
import {AppColors, Borders, Spacing} from '@styles/';
import commonStyles from '@styles/commonStyles';
import {SERVICE_TEXT} from '@constants/serviceData';

const ITEM_SIZE = 164;

const SERVICE_ICON = {
  [ServiceTypes.SHOPPING_COURIER]: require('../assets/Bag_icon.png'),
  [ServiceTypes.DELIVERY_COURIER]: require('../assets/Box_Icon.png'),
  [ServiceTypes.ORDER_COURIER]: require('../assets/Letter_icon.png'),
};

const SERVICE_ICON_WHITE = {
  [ServiceTypes.SHOPPING_COURIER]: require('@assets/images/icons/Order_15.png'),
  [ServiceTypes.DELIVERY_COURIER]: require('@assets/images/icons/Post.png'),
  [ServiceTypes.ORDER_COURIER]: require('@assets/images/icons/Letter_15.png'),
};

interface Props {
  type: ServiceTypesLiteral;
}
export const HomeOrderItem: React.FC<Props> = ({type}) => {
  return (
    <View style={styles.container}>
      <LinearGradient
        colors={[AppColors.VIOLET, AppColors.VIOLET_LIGHT]}
        useAngle
        angle={137}
        style={styles.gradient}>
        <Image source={SERVICE_ICON[type]} style={styles.icon} />

        <View style={styles.header}>
          <View style={styles.info}>
            <Text style={[styles.infoText, commonStyles.boldText10]}>
              №234 22.08.21
            </Text>
            <Text style={[styles.infoText, commonStyles.boldText14]}>
              {SERVICE_TEXT[type]}
            </Text>
          </View>
        </View>
        <View style={styles.details}>
          <View style={styles.item}>
            <Image source={SERVICE_ICON_WHITE[type]} style={styles.itemIcon} />
            <Text style={[styles.itemText, commonStyles.mediumText]}>
              OZON {'\n'}
              1500<Text style={commonStyles.mediumText10}>.00 ₽</Text>
            </Text>
          </View>
          <View style={styles.item}>
            <Image
              source={require('@assets/images/icons/Ruble_15.png')}
              style={styles.itemIcon}
            />
            <Text style={[styles.itemText, commonStyles.mediumText]}>
              Стоимость {'\n'}
              500<Text style={commonStyles.mediumText10}>.00 ₽</Text>
            </Text>
          </View>
        </View>
      </LinearGradient>
    </View>
  );
};

const ICON_SIZE = 120;
const ICON_WHITE_SIZE = 15;

const styles = StyleSheet.create({
  container: {
    width: ITEM_SIZE,
    height: ITEM_SIZE,
    marginRight: Spacing.SCALE_10,
    ...Platform.select({
      ios: {
        shadowColor: AppColors.GRAY_SHADOW,
        shadowOffset: {
          width: 0,
          height: 0,
        },
        shadowRadius: 6,
      },
      android: {
        elevation: 3,
      },
    }),
  },
  gradient: {
    flex: 1,
    borderRadius: Borders.RADIUS_22,
    padding: Spacing.SCALE_6,
  },

  info: {
    flex: 1,
    justifyContent: 'center',
    marginLeft: 70,
  },
  infoText: {
    color: AppColors.WHITE,
  },
  header: {
    flexDirection: 'row',
    marginTop: Spacing.SCALE_20,
    marginBottom: Spacing.SCALE_10,
  },
  icon: {
    position: 'absolute',
    top: -15,
    left: -20,
    width: ICON_SIZE,
    height: ICON_SIZE,
  },
  details: {
    backgroundColor: 'rgba(0, 0, 0,.15)',
    borderRadius: Borders.RADIUS_15,
    height: 77,
    width: 152,
    position: 'absolute',
    left: 6,
    bottom: 6,
    padding: Spacing.SCALE_10,
  },
  item: {
    flexDirection: 'row',
    marginBottom: 3,
  },
  itemIcon: {
    width: ICON_WHITE_SIZE,
    height: ICON_WHITE_SIZE,
    marginRight: Spacing.SCALE_5,
    top: 4,
  },
  itemText: {
    color: AppColors.WHITE,
  },
});
