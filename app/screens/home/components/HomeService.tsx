import * as React from 'react';
import {View, Text, Image, StyleSheet, TouchableOpacity} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import {AppColors, Borders, Spacing} from '@styles/';
import {ServiceTypesLiteral} from '@customTypes/service';
import {
  SERVICE_DESCRIPTION,
  SERVICE_ICON,
  SERVICE_IMAGE,
  SERVICE_TEXT,
} from '@constants/serviceData';
import commonStyles from '@styles/commonStyles';

interface Props {
  type: ServiceTypesLiteral;
  onPress?(): void;
}

export const HomeService: React.FC<Props> = ({type, onPress}) => {
  const icon = SERVICE_ICON[type];
  return (
    <TouchableOpacity onPress={onPress} style={styles.container}>
      <LinearGradient
        colors={[AppColors.BLUE, AppColors.SKYBLUE]}
        useAngle={true}
        angle={148}
        style={styles.gradient}>
        <View style={styles.info}>
          <Image style={styles.icon} source={icon} />
          <Text style={[styles.title, commonStyles.boldText26]}>
            {SERVICE_DESCRIPTION[type]}
          </Text>
        </View>
        <Image style={styles.image} source={SERVICE_IMAGE[type]} />
        <Text style={[styles.typeText, commonStyles.boldText14]}>
          {SERVICE_TEXT[type]}
        </Text>
      </LinearGradient>
    </TouchableOpacity>
  );
};

const CONTAINER_HEIGHT = 150;
const ICON_SIZE = 55;
const styles = StyleSheet.create({
  container: {
    height: CONTAINER_HEIGHT,
    marginBottom: Spacing.SCALE_10,
  },
  gradient: {
    borderRadius: Borders.RADIUS_22,
    flex: 1,
    padding: Spacing.SCALE_12,
  },
  info: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: Spacing.SCALE_12,
  },
  icon: {
    width: ICON_SIZE,
    height: ICON_SIZE,
  },
  image: {
    position: 'absolute',
    bottom: Spacing.SCALE_12,
    left: Spacing.SCALE_12,
  },
  title: {
    width: 200,
    color: AppColors.WHITE,
    marginLeft: Spacing.SCALE_5,
  },
  typeText: {
    position: 'absolute',
    color: AppColors.WHITE,
    right: Spacing.SCALE_16,
    bottom: Spacing.SCALE_12,
  },
  content: {
    flex: 1,
  },
});
