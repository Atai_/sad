import * as React from 'react';
import {View, ScrollView, StyleSheet} from 'react-native';

import {ServiceTypes} from '@customTypes/service';
import {Spacing} from '@styles/';

import {HomeHeader} from './HomeHeader';
import {HomeOrderItem} from './HomeOrderItem';

export const HomeOrders: React.FC = () => {
  return (
    <View style={styles.container}>
      <HomeHeader title="Заказы" description="Можно посмотреть и повторить" />
      <ScrollView horizontal showsHorizontalScrollIndicator={false}>
        <HomeOrderItem type={ServiceTypes.SHOPPING_COURIER} />
        <HomeOrderItem type={ServiceTypes.DELIVERY_COURIER} />

        <HomeOrderItem type={ServiceTypes.ORDER_COURIER} />
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginBottom: Spacing.SCALE_20,
  },
});
