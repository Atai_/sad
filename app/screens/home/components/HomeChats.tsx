import * as React from 'react';
import { View, Image, StyleSheet } from 'react-native';
import { Spacing } from '@styles/';

const CHAT_SIZE = 60;
const CHAT_ICON_SIZE = 20;

interface Props {
    avatar?: string,
}

export const HomeChats: React.FC<Props> = ({ avatar }) => {
    return (
        <View style={styles.container}>
            <View style={styles.chatItems}>
                <Image
                    source={require(avatar) || require('@/assets/images/icons/Smile1@3x.png')}
                    style={styles.avatar}
                />
                <Image
                    source={require('@assets/images/tabs/Messages_Red_Icon@3x.png')}
                    style={styles.chatIcon}
                />
            </View>
            <View style={styles.chatItems}>
                <Image
                    source={require(avatar) || require('@/assets/images/icons/Smile2@3x.png')}
                    style={styles.avatar}
                />
                <Image
                    source={require('@assets/images/tabs/Messages_Red_Icon@3x.png')}
                    style={styles.chatIcon}
                />
            </View>
            <View style={styles.chatItems}>
                <Image
                    source={require(avatar) || require('@/assets/images/icons/Smile3@3x.png')}
                    style={styles.avatar}
                />
                <Image
                    source={require('@assets/images/tabs/Messages_Red_Icon@3x.png')}
                    style={styles.chatIcon}
                />
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        flexDirection: 'column'
    },
    chatItems: {
        width: CHAT_SIZE,
        height: CHAT_SIZE,
        position: "relative",
        marginBottom: Spacing.SCALE_8,
    },
    avatar: {
        width: "100%",
        height: "100%",
        borderRadius: 30
    },
    chatIcon: {
        position: 'absolute',
        width: CHAT_ICON_SIZE,
        height: CHAT_ICON_SIZE,
        zIndex: 1,
        top: 0,
        right: 0
    },
});
