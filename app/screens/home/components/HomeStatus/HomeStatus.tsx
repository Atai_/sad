import * as React from 'react';
import { ScrollView, StyleSheet } from 'react-native';

import { Spacing } from '@styles/';
import { HomeStatusItemText } from './components/HomeStatusItemText';
import { HomeStatusItemCode } from './components/HomeStatusItemCode';
import { HomeStatusItemMap } from './components/HomeOrderItemMap';
import { HomeStatusSearchCourier } from './components/HomeStatusSearchCourier'
import { OrderApi } from '@services/api/OrderApi';
import { HomeStatusCourier } from './components/HomeStatusCourier';

const home_services_list = [
  {
    type: "shopping_courier",
    name: "Помощник покупок",
  },
  {
    type: "delivery_courier",
    name: "Просто курьер",
  },
  {
    type: "order_courier",
    name: "Доставщик заказов",
  },
];

export const HomeStatus: React.FC = () => {
  const [order, setOrder] = React.useState();

  const getOrder = () => {
    return OrderApi.getOrder().then(({ data }) => {
      setOrder(data);
    });
  };

  // const orderTypeName = () => {
  //   return (
  //     home_services_list.find((item) => item.type === order?.order_type)
  //       ?.name || "Помощник покупок"
  //   );
  // }

  React.useEffect(() => {
    getOrder();
  }, []);

  return (
    <ScrollView
      style={styles.container}
      contentContainerStyle={styles.content}
      horizontal
      showsHorizontalScrollIndicator={false}>
      {/* поиск курьера */}
      <HomeStatusSearchCourier
        icon={require('@assets/images/icons/Choosing_courier.png')}
        title="В поисках курьера"
        description={
          'Помощник покупок \nСтоличные аптеки 3000.00 ₽\nДоставка в 15:00 - 16:00 сегодня'
        }
      />
      {/* курьер */}
      <HomeStatusCourier
        // изменить икноку
        icon={require('@assets/images/icons/Choosing_courier.png')}
        title="Курьер Алексей"
        description={
          'Помощник покупок \nСтоличные аптеки 3000.00 ₽ \nБудет на первой точке ≈ 15:45 - 16:00'
        }
      />
      {/* карта */}
      <HomeStatusItemMap />
      {/* сумма */}
      <HomeStatusItemText
        // сменить иконку
        icon={require('@assets/images/icons/Lock_blue_icon_60.png')}
        title="Увеличь сумму по заказу"
        description={
          'Курьер запросил увеличение суммы на покупки. Нажми здесь и подтверди увеличение'
        }
      />
      {/* сменился курьер */}
      <HomeStatusItemText
        // сменить иконку
        icon={require('@assets/images/icons/Lock_blue_icon_60.png')}
        title="Сменился курьер"
        description={
          'Контакты нового скоро будут \nПомощник покупок Столичные аптеки 3000.00 ₽'
        }
      />
      {/* код курьера */}
      <HomeStatusItemCode code={'1234'} amount={1000} cash={0} />
      {/* проблема с оплатой */}
      <HomeStatusItemText
        icon={require('@assets/images/icons/Alarm_icon_кув_60.png')}
        title="Проблема с оплатой"
        description={
          'У тебя есть неоплаченный заказ. Нажми тут, чтобы провести платеж. Новые заказы недоступны для оформления.'
        }
      />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {},
  content: {
    padding: Spacing.SCALE_16,
  },
});
