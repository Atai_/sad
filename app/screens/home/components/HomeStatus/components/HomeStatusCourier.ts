import {AppColors, Spacing} from '@styles/';
import commonStyles from '@styles/commonStyles';
import * as React from 'react';
import {
  Image,
  Text,
  View,
  StyleSheet,
  ImageSourcePropType,
  TouchableOpacity,
} from 'react-native';
import {HomeStatusItem} from './HomeStatusItem';

interface Props {
  icon: ImageSourcePropType;
  iconPosition?: 'center' | 'top';
  title: string;
  description: string;
  onPress?(): void;
}

export const HomeStatusCourier: React.FC<Props> = ({
  icon,
  title,
  description,
  onPress,
  iconPosition = 'top',
}) => {
  const alignment = iconPosition === 'center' ? 'center' : 'flex-start';
  return (
    <HomeStatusItem>
      <View style={[styles.container]}>
        <View
          style={{
            justifyContent: alignment,
          }}>
          <Image source={icon} style={styles.icon} />
        </View>
        <View style={styles.info}>
          <View style={styles.text}>
            <Text style={[styles.title, commonStyles.boldText18]}>{title}</Text>
            <Text style={[styles.description, commonStyles.regularText]}>
              {description}
            </Text>
          </View>
        </View>
      </View>
    </HomeStatusItem>
  );
};
const ICON_WIDTH = 25;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
  },
  icon: {
    width: ICON_WIDTH,
    height: ICON_WIDTH,
    marginRight: Spacing.SCALE_10,
    resizeMode: 'contain',
  },
  info: {
    flex: 1,
  },
  button: {
    marginTop: Spacing.SCALE_5,
  },
  buttonText: {
    color: AppColors.WHITE,
  },
  title: {
    color: AppColors.WHITE,
    marginBottom: Spacing.SCALE_4,
  },
  description: {
    color: AppColors.WHITE,
  },
  text: {
    flex: 1,
  },
});
