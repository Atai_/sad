import {AppColors, Spacing} from '@styles/';
import commonStyles from '@styles/commonStyles';
import * as React from 'react';
import {Image, Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import {HomeStatusItem} from './HomeStatusItem';

interface Props {
  amount: number;
  cash: number;
  code: number | string;
  onPress?(): void;
}

export const HomeStatusItemCode: React.FC<Props> = ({
  amount,
  code,
  cash,
  onPress,
}) => {
  return (
    <HomeStatusItem>
      <View style={[styles.container]}>
        <Image
          source={require('@assets/images/icons/Lock_icon.png')}
          style={styles.icon}
        />
        <View style={styles.info}>
          <View style={styles.text}>
            <View style={styles.codeContainer}>
              <View style={styles.titleContainer}>
                <Text style={[styles.title, commonStyles.boldText18]}>
                  Код для исполнителя
                </Text>
                <Text style={[styles.description, commonStyles.regularText]}>
                  Не сообщай до получения доставки
                </Text>
              </View>
              <View style={styles.code}>
                <Text style={[styles.codeText, commonStyles.boldText30]}>
                  {code}
                </Text>
              </View>
            </View>
            <View style={styles.amountContainer}>
              <View style={styles.amount}>
                <Text style={[styles.amountText, commonStyles.regularText10]}>
                  Сумма покупок
                </Text>
                <Text style={[styles.amountValue, commonStyles.boldText10]}>
                  {amount.toFixed(2)} ₽
                </Text>
              </View>
              <View style={styles.amount}>
                <Text style={[styles.amountText, commonStyles.regularText10]}>
                  К оплате наличными
                </Text>
                <Text style={[styles.amountValue, commonStyles.boldText10]}>
                  {cash.toFixed(2)} ₽
                </Text>
              </View>
            </View>
          </View>

          <TouchableOpacity
            onPress={onPress}
            style={styles.button}
            hitSlop={{
              top: 10,
              bottom: 10,
              left: 10,
              right: 10,
            }}>
            <Text style={[styles.buttonText, commonStyles.boldText12]}>
              Перейти в заказ
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </HomeStatusItem>
  );
};
const ICON_WIDTH = 25;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
  },
  icon: {
    width: ICON_WIDTH,
    height: ICON_WIDTH,
    marginRight: Spacing.SCALE_10,
    resizeMode: 'contain',
  },
  info: {
    flex: 1,
  },
  button: {
    marginTop: Spacing.SCALE_5,
  },
  buttonText: {
    color: AppColors.WHITE,
  },
  title: {
    color: AppColors.WHITE,
  },
  description: {
    color: AppColors.WHITE,
  },
  text: {
    flex: 1,
  },
  codeContainer: {
    flexDirection: 'row',
    paddingBottom: Spacing.SCALE_7,
    borderBottomColor: AppColors.WHITE,
    borderBottomWidth: 1,
  },
  titleContainer: {
    flex: 1,
    marginRight: Spacing.SCALE_5,
  },
  code: {
    backgroundColor: 'rgba(0, 0, 0, 0.15)',
    width: 85,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: Spacing.SCALE_10,
  },
  codeText: {
    color: AppColors.WHITE,
  },
  amountContainer: {},
  amount: {
    flexDirection: 'row',
  },
  amountText: {
    color: AppColors.WHITE,
    flex: 1,
  },
  amountValue: {
    marginLeft: Spacing.SCALE_4,
    color: AppColors.WHITE,
  },
});
