import * as React from 'react';
import {View, Text, StyleSheet, Platform} from 'react-native';
import MapView, {Marker} from 'react-native-maps';
import {AppColors, Borders, Spacing} from '@styles/';
import commonStyles from '@styles/commonStyles';
import {ScreenDimensions} from '@constants/';
import {MARKER_ICONS} from '@constants/markerIcons';

interface Props {
  // type: ServiceTypesLiteral;
}

const location = {latitude: 37.78825, longitude: -122.4324};
const location2 = {latitude: 37.78825, longitude: -122.4326};
export const HomeStatusItemMap: React.FC<Props> = ({}) => {
  return (
    <View style={styles.container}>
      <MapView
        initialRegion={{
          latitude: 37.78825,
          longitude: -122.4324,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.034,
        }}
        provider="google"
        style={styles.map}>
        <Marker coordinate={location} icon={MARKER_ICONS.PERFORMER} />
        <Marker coordinate={location2} icon={MARKER_ICONS.MY_LOCATION} />
      </MapView>
      <Text style={[styles.title, commonStyles.boldText16]}>
        Алексей в пути
      </Text>
    </View>
  );
};

const CONTAINER_HEIGHT = 111;
const CONTAINER_WIDTH = Math.max(ScreenDimensions.width - Spacing.SCALE_16 * 2);

const styles = StyleSheet.create({
  container: {
    width: CONTAINER_WIDTH,
    height: CONTAINER_HEIGHT,
    marginRight: Spacing.SCALE_10,
    borderRadius: Borders.RADIUS_22,
    overflow: 'hidden',
    ...Platform.select({
      ios: {
        shadowColor: AppColors.GRAY_SHADOW,
        shadowOffset: {
          width: 0,
          height: 0,
        },
        shadowRadius: 6,
      },
      android: {
        elevation: 3,
      },
    }),
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  title: {
    color: AppColors.WHITE,
    paddingHorizontal: Spacing.SCALE_10,
    paddingVertical: Spacing.SCALE_4,
    borderRadius: Borders.RADIUS_11,
    overflow: 'hidden',
    backgroundColor: 'rgba(0, 0, 0, 0.15)',
    position: 'absolute',
    top: Spacing.SCALE_12,
    left: Spacing.SCALE_12,
  },
});
