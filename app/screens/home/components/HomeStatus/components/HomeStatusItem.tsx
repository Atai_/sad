import {ScreenDimensions} from '@constants/';
import {AppColors, Borders, Spacing} from '@styles/';
import * as React from 'react';
import {StyleSheet, View} from 'react-native';

import LinearGradient from 'react-native-linear-gradient';

export const HomeStatusItem: React.FC = ({children}) => {
  return (
    <View style={styles.container}>
      <LinearGradient
        colors={[AppColors.SKYBLUE, '#B215EA']}
        useAngle
        angle={151}
        style={styles.gradient}>
        {children}
      </LinearGradient>
    </View>
  );
};

const CONTAINER_HEIGHT = 111;
const CONTAINER_WIDTH = Math.max(ScreenDimensions.width - Spacing.SCALE_16 * 2);

const styles = StyleSheet.create({
  container: {
    height: CONTAINER_HEIGHT,
    width: CONTAINER_WIDTH,
    marginRight: Spacing.SCALE_10,
  },
  gradient: {
    flex: 1,
    borderRadius: Borders.RADIUS_22,
    paddingVertical: Spacing.SCALE_12,
    paddingHorizontal: Spacing.SCALE_12,
  },
});
