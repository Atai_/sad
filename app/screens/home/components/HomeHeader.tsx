import {AppColors, Spacing} from '@styles/';
import commonStyles from '@styles/commonStyles';
import * as React from 'react';
import {StyleSheet, View, Text} from 'react-native';

interface Props {
  title: string;
  description: string;
}
export const HomeHeader: React.FC<Props> = ({title, description}) => {
  return (
    <View style={styles.container}>
      <Text
        style={[
          styles.title,
          commonStyles.boldText30,
          commonStyles.textShadowBlack16,
        ]}>
        {title}
      </Text>
      <Text style={[styles.description, commonStyles.regularText14]}>
        {description}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginBottom: Spacing.SCALE_10,
  },
  title: {
    color: AppColors.BLACK_MEDIUM,
  },
  description: {
    color: AppColors.GRAY_TEXT,
  },
});
