import * as React from 'react';
import {
  ScrollView,
  StyleSheet,
  Image,
  View,
  Text,
  InteractionManager,
} from 'react-native';
import {SafeAreaView, useSafeAreaInsets} from 'react-native-safe-area-context';
import Modal from 'react-native-modal';
import BottomSheet, {BottomSheetModal} from '@gorhom/bottom-sheet';

import commonStyles from '@styles/commonStyles';
import {AppColors, Spacing} from '@styles/';
import LinearGradient from 'react-native-linear-gradient';
import {AppBackButton} from '@components/AppBackButton/AppBackButton';
import {AppBottomSheetHeader} from '@components/AppBottomSheet/AppBottomSheetHeader';
import {StackScreenProps} from '@react-navigation/stack';
import {ServiceTypes, ServiceTypesLiteral} from '@customTypes/service';
import {SERVICE_DESCRIPTION, SERVICE_ICON} from '@constants/serviceData';
import {HomeServiceShoppingCourier} from './components/HomeServiceShoppingCourier';
import {HomeServiceCreateHeader} from './components/HomeServiceCreateHeader';
import {HomeServiceSheet} from './components/HomeServiceSheet';
import {HomeServiceAddresses} from './components/HomeServicesAddresses';
import {NAVIGATION_HOME} from '@navigation/screenNames';
import {AppModalSelect} from '@components/AppModalSelect';
import {HomeServiceWhereBuyModal} from './components/HomeServiceWhereBuyModal';
const GRADIENT_HEIGHT = 199;

const AMOUNT_LIST = [
  {
    title: '1 000 ₽',
    value: 1000,
  },
  {
    title: '2 000 ₽',
    value: 2000,
  },
  {
    title: '3 000 ₽',
    value: 3000,
  },
  {
    title: '4 000 ₽',
    value: 4000,
  },
  {
    title: '5 000 ₽',
    value: 5000,
  },
  {
    title: '6 000 ₽',
    value: 6000,
  },
];
export const HomeServiceCreateScreen: React.FC<StackScreenProps<any>> = ({
  route,
  navigation,
}) => {
  const insets = useSafeAreaInsets();
  const bottomSheetModalRef = React.useRef<BottomSheetModal>(null);
  const bottomSheetWhereBuyRef = React.useRef<BottomSheet>(null);
  const [type, setType] = React.useState<ServiceTypesLiteral>(
    route.params?.type || ServiceTypes.SHOPPING_COURIER,
  );
  const [visible, setVisible] = React.useState<boolean>(false);

  const snapPoints = React.useMemo(() => ['100%'], []);

  const containerStyle = {
    paddingTop: Spacing.SCALE_10 + insets.top,
  };
  const openAddressSheet = () => {
    bottomSheetModalRef.current?.present();
  };
  const onSelectOpenWherebuy = () => {
    bottomSheetWhereBuyRef.current?.snapToIndex(0);
  };
  const onPressMap = () => {
    bottomSheetModalRef.current?.close();
    InteractionManager.runAfterInteractions(() => {
      navigation.push(NAVIGATION_HOME.SELECT_ADDRESS);
    });
  };

  return (
    <SafeAreaView edges={['bottom']} style={commonStyles.containerWhite}>
      <View style={styles.content}>
        <View style={styles.gradientContainer}>
          <LinearGradient
            colors={[AppColors.BLUE, AppColors.SKYBLUE]}
            useAngle
            angle={153}
            style={[styles.gradient, containerStyle]}>
            <View style={styles.header}>
              <AppBackButton />
              <Image style={styles.headerIcon} source={SERVICE_ICON[type]} />
              <Text style={[styles.headerText, commonStyles.boldText26]}>
                {SERVICE_DESCRIPTION[type]}
              </Text>
            </View>
            <View style={styles.sheetHeader}>
              <AppBottomSheetHeader />
            </View>
          </LinearGradient>
        </View>

        <ScrollView bounces={false} style={[styles.scroll]}>
          <View style={styles.scrollContent}>
            <HomeServiceCreateHeader active={type} onPress={setType} />

            <HomeServiceShoppingCourier
              onSelectOpenWherebuy={onSelectOpenWherebuy}
              onSelectAddress={openAddressSheet}
            />
          </View>
        </ScrollView>
      </View>
      <View style={styles.button}></View>
      <BottomSheetModal
        handleComponent={() => <AppBottomSheetHeader title="Где купить" />}
        ref={bottomSheetModalRef}
        snapPoints={snapPoints}
        topInset={insets.top}>
        <HomeServiceSheet
          onPressAddress={() => {
            setVisible(true);
          }}
          onPressMap={onPressMap}
        />
      </BottomSheetModal>
      <Modal
        isVisible={visible}
        onDismiss={() => setVisible(false)}
        onSwipeComplete={() => setVisible(false)}
        swipeDirection="down"
        propagateSwipe
        style={styles.bottomModal}>
        <HomeServiceAddresses />
      </Modal>
      <AppModalSelect
        title="На какую сумму?"
        description="Без переплат за покупки. Точно по чеку. Не более 6 000 ₽"
        visible={false}
        data={AMOUNT_LIST}
      />
      <HomeServiceWhereBuyModal ref={bottomSheetWhereBuyRef} />
    </SafeAreaView>
  );
};
const ICON_SIZE = 55;
const styles = StyleSheet.create({
  content: {
    flex: 1,
    backgroundColor: AppColors.WHITE_GRAY,
  },
  button: {
    paddingVertical: Spacing.SCALE_6,
    paddingHorizontal: Spacing.SCALE_16,
    backgroundColor: AppColors.WHITE,
  },
  gradientContainer: {
    height: GRADIENT_HEIGHT,
  },
  gradient: {
    flex: 1,
    paddingBottom: Spacing.SCALE_10,
  },
  gradientContent: {},
  scroll: {
    flex: 1,
  },
  scrollContent: {
    paddingHorizontal: Spacing.SCALE_16,
  },

  header: {
    flexDirection: 'row',
    paddingHorizontal: Spacing.SCALE_16,
  },
  headerText: {
    color: AppColors.WHITE,
    marginLeft: Spacing.SCALE_5,
    width: 200,
  },
  headerIcon: {
    width: ICON_SIZE,
    height: ICON_SIZE,
    marginLeft: Spacing.SCALE_35,
  },
  sheetHeader: {
    bottom: 0,
    left: 0,
    position: 'absolute',
    width: '100%',
  },

  bottomModal: {
    flexDirection: 'column',
    justifyContent: 'flex-end',
    margin: 0,
  },
});
