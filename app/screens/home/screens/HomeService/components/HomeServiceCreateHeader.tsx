import * as React from 'react';
import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';

import {SERVICE_TEXT} from '@constants/serviceData';
import {ServiceTypes, ServiceTypesLiteral} from '@customTypes/service';
import {AppColors, Borders, Spacing} from '@styles/';
import commonStyles from '@styles/commonStyles';

const HEADER_LIST_ICONS = {
  [ServiceTypes.SHOPPING_COURIER]: require('../assets/Bag_color_30.png'),
  [ServiceTypes.SHOPPING_COURIER +
  'active']: require('../assets/Bag_color_30.png'),
  [ServiceTypes.DELIVERY_COURIER]: require('../assets/Letter_bw_30.png'),
  [ServiceTypes.DELIVERY_COURIER +
  'active']: require('../assets/Letter_bw_30.png'),
  [ServiceTypes.ORDER_COURIER]: require('../assets/Box_bw_30.png'),
  [ServiceTypes.ORDER_COURIER + 'active']: require('../assets/Box_bw_30.png'),
};

interface Props {
  active: ServiceTypesLiteral;
  onPress?(type: ServiceTypes): void;
}

export const HomeServiceCreateHeader: React.FC<Props> = ({active, onPress}) => {
  const keys = Object.keys(ServiceTypes) as Array<keyof typeof ServiceTypes>;
  return (
    <View style={styles.container}>
      {keys.map((key, index) => {
        const type = ServiceTypes[key];
        const isActive = active === type;
        const icon = HEADER_LIST_ICONS[`${type}${isActive ? 'active' : ''}`];
        const isLast = index === keys.length - 1;
        return (
          <TouchableOpacity
            onPress={() => onPress && onPress(type)}
            style={[styles.item, isLast ? styles.itemLast : null]}>
            <Image source={icon} style={styles.icon} />
            <Text
              style={[
                styles.text,
                isActive ? styles.textActive : null,
                commonStyles.boldText10,
              ]}>
              {SERVICE_TEXT[type]}
            </Text>
          </TouchableOpacity>
        );
      })}
    </View>
  );
};

const ITEM_HEIGHT = 69;
const ICON_SIZE = 30;
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginBottom: Spacing.SCALE_20,
  },
  item: {
    flex: 1,
    height: ITEM_HEIGHT,
    backgroundColor: AppColors.WHITE,
    marginRight: Spacing.SCALE_10,
    borderRadius: Borders.RADIUS_11,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    paddingHorizontal: Spacing.SCALE_8,
  },
  itemLast: {
    marginRight: 0,
  },
  text: {
    color: AppColors.GRAY_TEXT,
  },
  textActive: {
    color: AppColors.SKYBLUE,
  },
  icon: {
    width: ICON_SIZE,
    height: ICON_SIZE,
    marginRight: Spacing.SCALE_6,
  },
});
