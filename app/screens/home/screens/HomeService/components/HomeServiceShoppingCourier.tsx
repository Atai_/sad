import {ButtonIcon} from '@components/ButtonIcon';
import {ButtonText} from '@components/ButtonText';
import {ShoppingCourierPlaces} from '@customTypes/services';
import {Spacing} from '@styles/';
import * as React from 'react';
import {StyleSheet, View} from 'react-native';

interface Props {
  onSelectAddress(): void;
  onSelectOpenWherebuy: () => void;
}

export const HomeServiceShoppingCourier: React.FC<Props> = ({
  onSelectAddress,
  onSelectOpenWherebuy,
}) => {
  const [places, setPlaces] = React.useState<ShoppingCourierPlaces>([
    {
      amount: 1000,
      address: null,
    },
  ]);
  const [deliveryTime, setDeliveryTime] = React.useState(null);
  const isMaximumPlaces = places.length < 3;
  const addPlace = () => {
    if (isMaximumPlaces) {
      setPlaces((prev) => [
        ...prev,
        {
          amount: 1000,
          address: null,
        },
      ]);
    }
  };
  const removePlace = (index: number) => {
    const placeList = places.slice();
    placeList.splice(index, 1);
    setPlaces(placeList);
  };
  const deliveryTimeValue = deliveryTime ? '11:00' : 'Как можно скорее';
  return (
    <>
      <View>
        {places.map((item, index) => (
          <React.Fragment key={index}>
            <ButtonText
              onPress={onSelectAddress}
              containerStyle={styles.input}
              placeholder="Где купить?"
              icon={require('@assets/images/icons/Shop_blue_20.png')}
              iconTextBottom={places.length > 1 ? `${index + 1}` : ''}
              iconRight={
                places.length > 1
                  ? require('@assets/images/icons/Delete_grey_20.png')
                  : null
              }
              iconRightOnPress={() => removePlace(index)}
            />
            <View style={styles.buttonContainer}>
              <ButtonText
                containerStyle={[styles.buttonInput, styles.input]}
                placeholder="Примерная сумма покупки?"
                value="1000 Р"
                icon={require('@assets/images/icons/Rubble_blue_20.png')}
              />
              {index === places.length - 1 && isMaximumPlaces ? (
                <ButtonIcon
                  icon={require('@assets/images/icons/Plus_blue_20.png')}
                  title={'Добавить\nточку'}
                  onPress={addPlace}
                />
              ) : null}
            </View>
          </React.Fragment>
        ))}
        <ButtonText
          onPress={onSelectOpenWherebuy}
          containerStyle={styles.input}
          placeholder="Что купить?"
          icon={require('@assets/images/icons/List_blue_20.png')}
        />
        <ButtonText
          containerStyle={styles.input}
          placeholder="Куда доставить?"
          icon={require('@assets/images/icons/Location_blue_20.png')}
        />
        <ButtonText
          containerStyle={styles.input}
          placeholder="Время доставки?"
          value={deliveryTimeValue}
          icon={require('@assets/images/icons/Time_blue_20.png')}
        />
        <ButtonText
          containerStyle={styles.input}
          placeholder="Помочь с заказом"
          icon={require('@assets/images/icons/Help_gray_20.png')}
        />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  input: {
    marginBottom: Spacing.SCALE_5,
  },
  buttonContainer: {
    flexDirection: 'row',
    marginBottom: Spacing.SCALE_15,
  },
  buttonInput: {
    marginRight: Spacing.SCALE_5,
    flex: 1,
  },
});
