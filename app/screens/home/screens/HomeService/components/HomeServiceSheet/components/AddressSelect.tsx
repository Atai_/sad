import * as React from 'react';
import {
  ImageSourcePropType,
  StyleSheet,
  Image,
  TouchableOpacity,
  Text,
  StyleProp,
  ViewStyle,
} from 'react-native';
import {AppColors, Spacing} from '@styles/';
import commonStyles from '@styles/commonStyles';
interface Props {
  title: string;
  icon?: ImageSourcePropType;
  onPress?(): void;
  containerStyle?: StyleProp<ViewStyle>;
}

export const AddressSelect: React.FC<Props> = ({
  title,
  onPress,
  icon,
  containerStyle,
}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={[styles.container, containerStyle]}>
      {icon ? <Image style={styles.icon} source={icon} /> : null}
      <Text style={[commonStyles.boldText12, styles.title]}>{title}</Text>
    </TouchableOpacity>
  );
};

const CONTAINER_SIZE = 74;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderRadius: 10,
    height: CONTAINER_SIZE,
    backgroundColor: AppColors.WHITE,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: Spacing.SCALE_40,
  },
  title: {
    color: AppColors.SKYBLUE,
    textAlign: 'center',
  },
  icon: {},
});
