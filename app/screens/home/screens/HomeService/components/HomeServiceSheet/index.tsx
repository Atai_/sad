import * as React from 'react';
import {ScrollView, StyleSheet, View, Text} from 'react-native';
import {AddressSelect} from './components/AddressSelect';
import {AppColors, Spacing} from '@styles/';
import commonStyles from '@styles/commonStyles';
import {ButtonText} from '@components/ButtonText';

interface Props {
  onPressAddress(): void;
  onPressMap(): void;
}
export const HomeServiceSheet: React.FC<Props> = ({
  onPressAddress,
  onPressMap,
}) => {
  return (
    <ScrollView contentContainerStyle={styles.content} style={styles.sheet}>
      <View style={[styles.header, styles.block]}>
        <AddressSelect
          title="Выбрать на карте"
          containerStyle={styles.headerItemFirst}
          onPress={onPressMap}
        />
        <AddressSelect onPress={onPressAddress} title="Знаю точный адрес" />
      </View>

      <View style={styles.block}>
        <Text
          style={[
            styles.title,
            commonStyles.boldText24,
            commonStyles.textShadowBlack16,
          ]}>
          Мои места
        </Text>
        <ButtonText
          containerStyle={styles.item}
          reverse
          value="Без названия"
          placeholder="Красная площадь, д.1"
          icon={require('@assets/images/icons/Location_blue_20.png')}
        />
        <ButtonText
          containerStyle={styles.item}
          reverse
          value="Красное и белое"
          placeholder="Красная площадь, д.1"
          icon={require('@assets/images/icons/Location_blue_20.png')}
        />
        <ButtonText
          reverse
          value="Пятерочка"
          placeholder="Красная площадь, д.1"
          icon={require('@assets/images/icons/Location_blue_20.png')}
        />
      </View>
      <Text
        style={[
          styles.title,
          commonStyles.boldText24,
          commonStyles.textShadowBlack16,
        ]}>
        Ближайшее ко мне
      </Text>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  sheet: {
    backgroundColor: AppColors.WHITE_GRAY,
    flex: 1,
  },
  header: {
    flexDirection: 'row',
  },
  headerItemFirst: {
    marginRight: Spacing.SCALE_10,
  },
  content: {
    paddingHorizontal: Spacing.SCALE_16,
  },
  title: {
    color: AppColors.BLACK,
    marginBottom: Spacing.SCALE_20,
  },
  item: {
    marginBottom: Spacing.SCALE_10,
  },
  block: {
    marginBottom: Spacing.SCALE_30,
  },
});
