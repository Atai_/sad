import * as React from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {AppBottomSheet} from '@components/AppBottomSheet';
import BottomSheet from '@gorhom/bottom-sheet';
import commonStyles from '@styles/commonStyles';
import {AppColors, Borders, Spacing} from '@styles/';
import {boxShadow} from '@styles/mixins';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {TextFieldSheet} from '@components/TextField/TextFieldSheet';
interface Props {
  defaultText?: string;
}

export const HomeServiceWhereBuyModal = React.forwardRef<BottomSheet, Props>(
  ({defaultText = ''}, ref) => {
    const insets = useSafeAreaInsets();
    const snapPoints = React.useMemo(() => ['100%'], []);
    const [text, setText] = React.useState(defaultText);
    return (
      <AppBottomSheet
        enablePanDownToClose
        enableOverDrag={false}
        keyboardBehavior="interactive"
        keyboardBlurBehavior="restore"
        topInset={insets.top}
        bottomInset={insets.bottom}
        index={-1}
        ref={ref}
        snapPoints={snapPoints}
        title="Что купить?">
        <View style={styles.content}>
          <Text style={[styles.description, commonStyles.mediumText]}>
            Товаров и цен здесь нет. Стоимость по чеку из магазина, никаких
            переплат. Добавь фото для исполнителя.
          </Text>
          <TextFieldSheet
            multiline
            style={styles.input}
            value={text}
            onChangeText={setText}
          />
        </View>
      </AppBottomSheet>
    );
  },
);

const INPUT_HEIGHT = 209;

const styles = StyleSheet.create({
  modal: {
    flexDirection: 'column',
    justifyContent: 'flex-end',
    margin: 0,
    flex: 1,
    backgroundColor: AppColors.WHITE_GRAY,
  },
  content: {
    paddingHorizontal: Spacing.SCALE_16,
    backgroundColor: AppColors.WHITE_GRAY,
    flex: 1,
  },
  description: {
    color: AppColors.GRAY_TEXT,
    marginBottom: Spacing.SCALE_10,
  },
  input: {
    padding: Spacing.SCALE_12,
    height: INPUT_HEIGHT,
    borderRadius: Borders.RADIUS_15,
    marginBottom: Spacing.SCALE_10,
    backgroundColor: AppColors.WHITE,
    color: AppColors.BLACK_MEDIUM,
    ...boxShadow({
      color: '#00000029',
      radius: 6,
    }),
    ...commonStyles.boldText14,
  },
});
