import * as React from 'react';
import {View, StyleSheet, ScrollView} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import debounce from 'lodash.debounce';
import {TextField} from '@components/TextField';
import {AppBottomSheetHeader} from '@components/AppBottomSheet/AppBottomSheetHeader';
import {AppColors, Borders, Spacing} from '@styles/';
import {useKeyboard} from '@hooks/useKeyboard';
import {GooglePlacesService} from '@services/google/GooglePlacesService';
import {ButtonText} from '@components/ButtonText';

export const HomeServiceAddresses: React.FC = () => {
  const [keyboard] = useKeyboard();
  const insets = useSafeAreaInsets();
  const [query, setQuery] = React.useState('');
  const [predictions, setPredictions] = React.useState<
    google.maps.places.AutocompletePrediction[]
  >([]);
  const debounceGetAddresses = React.useRef(
    debounce(
      (value: string) =>
        GooglePlacesService.getQueryPlaces(value)
          .then((response) => {
            console.log(response);

            setPredictions(response.data.predictions);
          })
          .catch((e) => {
            console.log(e.response);
          }),
      300,
    ),
  );

  React.useEffect(() => {
    if (query.length > 3) {
      debounceGetAddresses.current(query);
    } else {
      setPredictions([]);
    }
  }, [query]);
  return (
    <View
      style={[
        styles.container,
        {
          marginBottom: keyboard,
        },
      ]}>
      <AppBottomSheetHeader title="По точному адресу" />
      <TextField
        placeholder="Введи улицу и дом места покупки"
        value={query}
        onChangeText={setQuery}
        containerStyle={styles.input}
      />
      <ScrollView
        style={styles.scroll}
        contentContainerStyle={[
          styles.scrollContent,
          {paddingBottom: insets.bottom},
        ]}>
        {predictions.map((item) => {
          return (
            <ButtonText
              containerStyle={styles.item}
              key={item.place_id}
              icon={require('@assets/images/icons/Shop_blue_20.png')}
              value={item.structured_formatting.main_text}
            />
          );
        })}
      </ScrollView>
    </View>
  );
};
const CONTAINER_HEIGHT = 273;
const styles = StyleSheet.create({
  container: {
    height: CONTAINER_HEIGHT,
    backgroundColor: AppColors.WHITE_GRAY,
    borderTopRightRadius: Borders.RADIUS_22,
    borderTopLeftRadius: Borders.RADIUS_22,
  },
  input: {
    marginHorizontal: Spacing.SCALE_16,
  },
  scroll: {
    flex: 1,
  },
  scrollContent: {
    paddingTop: Spacing.SCALE_10,
    paddingHorizontal: Spacing.SCALE_16,
  },
  item: {
    marginBottom: Spacing.SCALE_10,
  },
});
