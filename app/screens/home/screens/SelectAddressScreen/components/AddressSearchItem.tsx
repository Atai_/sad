import * as React from 'react';
import {Text, TouchableOpacity, StyleSheet} from 'react-native';

import {ShopDummy} from '@customTypes/shop';
import {AppColors, Borders, Spacing} from '@styles/';
import commonStyles from '@styles/commonStyles';
import {boxShadow} from '@styles/mixins';

interface Props {
  shop: ShopDummy;
  onPress(): void;
}

export const AddressSearchItem: React.FC<Props> = ({shop, onPress}) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles.container}>
      <Text style={[styles.text, commonStyles.mediumText14]}>{shop.name}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    marginRight: Spacing.SCALE_10,
    paddingVertical: Spacing.SCALE_5,
    paddingHorizontal: Spacing.SCALE_16,
    borderRadius: Borders.RADIUS_8,
    backgroundColor: AppColors.WHITE,

    ...boxShadow({
      radius: 3,
      color: '#00000029',
    }),
  },
  text: {
    color: AppColors.GRAY_TEXT,
  },
});
