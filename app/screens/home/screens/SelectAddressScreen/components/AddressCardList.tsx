import * as React from 'react';
import {FlatList, ListRenderItem, StyleSheet} from 'react-native';
import {BottomSheetFlatList} from '@gorhom/bottom-sheet';
import {isPointInPolygon} from 'geolib';

import {getAddressCardStatus} from '../helpers/getCardStatus';
import {AddressCardItem} from './AddressCardItem';
import {getMKADCoordinates} from '@constants/workingBounds';
import {Spacing} from '@styles/';

export type GooglePlace = google.maps.places.PlaceResult;

interface Props {
  onPress: (place: GooglePlace) => void;
  addresses: GooglePlace[];
  selectedPlace: GooglePlace | null;
}

export const AddressCardList = React.forwardRef<FlatList, Props>(
  ({onPress, addresses, selectedPlace}, ref) => {
    const renderItem: ListRenderItem<GooglePlace> = React.useCallback(
      ({item}) => {
        const selected = item.place_id === selectedPlace?.place_id;
        const location = item.geometry
          ?.location as any as google.maps.LatLngLiteral;

        const working = isPointInPolygon(
          {
            latitude: location?.lat,
            longitude: location?.lng,
          },
          getMKADCoordinates(),
        );
        return (
          <AddressCardItem
            onPress={() => onPress(item)}
            name={item.name || ''}
            status={getAddressCardStatus(selected, working)}
            description={item.vicinity}
          />
        );
      },
      [onPress, selectedPlace?.place_id],
    );
    return (
      <BottomSheetFlatList
        ref={ref}
        horizontal
        showsHorizontalScrollIndicator={false}
        data={addresses}
        extraData={addresses}
        keyExtractor={(item) => item.place_id as string}
        contentContainerStyle={styles.content}
        renderItem={renderItem}
      />
    );
  },
);

const styles = StyleSheet.create({
  content: {
    paddingHorizontal: Spacing.SCALE_16,
    paddingVertical: Spacing.SCALE_20,
  },
});
