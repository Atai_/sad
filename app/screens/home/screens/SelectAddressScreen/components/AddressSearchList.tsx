import * as React from 'react';
import {FlatList, StyleSheet} from 'react-native';
import {AddressSearchItem} from './AddressSearchItem';

import {ShopDummy} from '@customTypes/shop';
import {Spacing} from '@styles/';

interface Props {
  onPress?: (shop: ShopDummy) => void;
  shops: ShopDummy[];
}

export const AddressSearchList: React.FC<Props> = ({shops, onPress}) => {
  return (
    <FlatList
      horizontal
      showsHorizontalScrollIndicator={false}
      data={shops}
      extraData={shops}
      keyExtractor={(item) => item.id.toString()}
      renderItem={({item}) => {
        return (
          <AddressSearchItem onPress={() => onPress?.(item)} shop={item} />
        );
      }}
      contentContainerStyle={styles.content}
      style={styles.container}
    />
  );
};

const styles = StyleSheet.create({
  container: {},
  content: {
    paddingHorizontal: Spacing.SCALE_16,
    flexDirection: 'row',
    justifyContent: 'center',
  },
});
