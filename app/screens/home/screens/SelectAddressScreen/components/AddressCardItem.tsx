import * as React from 'react';
import {
  View,
  StyleSheet,
  Text,
  TextStyle,
  TouchableOpacity,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import {AppColors, Borders, Spacing} from '@styles/';
import commonStyles from '@styles/commonStyles';
import {boxShadow} from '@styles/mixins';
interface Props {
  name: string;
  description?: string;
  onPress?(): void;
  status?: 'selected' | 'not_work' | 'not_selected';
}

export const AddressCardItem: React.FC<Props> = ({
  name,
  description,
  status = 'not_selected',
  onPress,
}) => {
  const gradients = {
    selected: [AppColors.VIOLET, AppColors.VIOLET_LIGHT],
    not_selected: [AppColors.WHITE, AppColors.WHITE],
    not_work: ['#BFC5C9', '#949F9F'],
  };

  const text = {
    selected: 'Подтвердить',
    not_selected: '',
    not_work: 'Пока не работаем',
  };
  const textColor = {
    selected: AppColors.VIOLET_TEXT,
    not_selected: AppColors.SKYBLUE,
    not_work: AppColors.GRAY_TEXT,
  };
  const currentText = text[status];
  const buttonTextStyle = {color: textColor[status]} as TextStyle;
  return (
    <TouchableOpacity onPress={onPress} style={styles.container}>
      <LinearGradient
        useAngle
        angle={180}
        colors={gradients[status]}
        style={styles.gradient}>
        <View style={styles.content}>
          <Text
            numberOfLines={1}
            style={[
              styles.title,
              commonStyles.boldText14,
              commonStyles.textShadowBlack16,
            ]}>
            {name}
          </Text>
          <Text
            numberOfLines={3}
            style={[styles.description, commonStyles.mediumText10]}>
            {description}
          </Text>
          <Text
            style={[
              styles.buttonText,
              commonStyles.boldText12,
              buttonTextStyle,
            ]}>
            {currentText}
          </Text>
        </View>
      </LinearGradient>
    </TouchableOpacity>
  );
};

const CARD_WIDTH = 147;
const CARD_HEIGHT = 90;

const styles = StyleSheet.create({
  container: {
    borderTopEndRadius: Borders.RADIUS_13,
    borderTopStartRadius: Borders.RADIUS_13,
    borderBottomEndRadius: Borders.RADIUS_13,
    borderBottomStartRadius: Borders.RADIUS_13,
    overflow: 'hidden',
    width: CARD_WIDTH,
    height: CARD_HEIGHT,
    marginRight: Spacing.SCALE_10,
    backgroundColor: 'transparent',
    ...boxShadow({
      radius: 3,
      color: '#00000029',
    }),
  },
  gradient: {
    width: '100%',
    height: '100%',
    borderColor: 'transparent',

    padding: Spacing.SCALE_4,
  },
  content: {
    backgroundColor: AppColors.WHITE,
    flex: 1,
    borderRadius: Borders.RADIUS_9,
    padding: Spacing.SCALE_6,
    position: 'relative',
  },
  title: {
    color: AppColors.BLACK_MEDIUM,
    marginBottom: Spacing.SCALE_5,
  },
  description: {
    color: AppColors.GRAY_TEXT,
  },
  buttonText: {
    textAlign: 'center',
    position: 'absolute',
    bottom: Spacing.SCALE_6,
    width: '100%',
    left: Spacing.SCALE_6,
    // backgroundColor: 'red',
  },
});
