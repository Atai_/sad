export const getAddressCardStatus = (selected: boolean, working?: boolean) => {
  if (selected && !working) {
    return 'not_work';
  }

  return selected ? 'selected' : 'not_selected';
};
