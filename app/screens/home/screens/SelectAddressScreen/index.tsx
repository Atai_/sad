import * as React from 'react';
import {
  StyleSheet,
  FlatList,
  View,
  Image,
  TouchableOpacity,
  Alert,
} from 'react-native';
import {SafeAreaView, useSafeAreaInsets} from 'react-native-safe-area-context';
import BottomSheet from '@gorhom/bottom-sheet';
import {AppBackButton} from '@components/AppBackButton/AppBackButton';
import {AppColors, Borders, Spacing} from '@styles/';
import MapView, {PROVIDER_GOOGLE, Region, Marker} from 'react-native-maps';
import {ScreenDimensions} from '@constants/';
import {AddressSearchList} from './components/AddressSearchList';
import {ShopDummy} from '@customTypes/shop';
import {ShopsApi} from '@services/api/ShopsApi';
import {TextFieldSheet} from '@components/TextField/TextFieldSheet';
import {AppBottomSheet} from '@components/AppBottomSheet/AppBottomSheet';
import {GooglePlacesService} from '@services/google/GooglePlacesService';

import {AddressCardList, GooglePlace} from './components/AddressCardList';
import {MARKER_ICONS} from '@constants/markerIcons';
const SHEET_HEIGHT = 200;

export const SelectAddressScreen: React.FC = () => {
  const insets = useSafeAreaInsets();
  const flatList = React.useRef<FlatList>(null);
  const mapView = React.useRef<MapView>(null);
  const sheet = React.useRef<BottomSheet>(null);
  const [addresses, setAddresses] = React.useState<GooglePlace[]>([]);
  const [selectedPlace, setSelectedPlace] = React.useState<GooglePlace | null>(
    null,
  );
  const [query, setQuery] = React.useState('');
  const [region, setRegion] = React.useState<Region>({
    latitude: 55.7558,
    longitude: 37.6173,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.034,
  });
  const [shopsDummy, setShopsDummy] = React.useState<ShopDummy[]>([]);

  const onSelectQuery = (text: string) => {
    setQuery(text);
  };
  const changeZoom = (type: 'plus' | 'minus') => {
    mapView.current?.getCamera().then((camera) => {
      if (type === 'plus') {
        camera.zoom += 1;
      } else {
        camera.zoom -= 1;
      }
      mapView.current?.animateCamera(camera);
    });
  };
  const onSelectAddress = (place: GooglePlace) => {
    const location = place.geometry
      ?.location as any as google.maps.LatLngLiteral;
    const addressIndex = addresses.findIndex(
      (item) => item.place_id === place.place_id,
    );
    mapView.current?.animateCamera({
      center: {
        longitude: location.lng,
        latitude: location.lat,
      },
      zoom: 15,
    });
    setSelectedPlace(place);

    if (addressIndex !== -1) {
      flatList.current?.scrollToIndex({
        animated: true,
        index: addressIndex,
        viewOffset: Spacing.SCALE_16,
      });
    }
  };

  const getList = () => {
    return ShopsApi.getDummyList().then(({data}) => {
      setShopsDummy(data);
    });
  };

  const onRegionChangeComplete = (data: Region) => {
    setRegion(data);
  };
  const snapPoints = React.useMemo(() => {
    return [SHEET_HEIGHT + insets.bottom];
  }, [insets.bottom]);

  const onNearbySearch = () => {
    GooglePlacesService.nearbySearch(query, {
      lat: region.latitude,
      lng: region.longitude,
    }).then(({data}) => {
      setAddresses(data.results);
    });
  };

  React.useEffect(() => {
    getList();
  }, []);

  return (
    <SafeAreaView edges={['bottom']} style={styles.container}>
      <View style={styles.content}>
        <View style={styles.controls}>
          <TouchableOpacity
            onPress={() => changeZoom('plus')}
            style={[styles.controlItem, styles.controlItemSpace]}>
            <Image source={require('@assets/images/map/PlusButton.png')} />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => changeZoom('minus')}
            style={[styles.controlItem, styles.controlItemSpace]}>
            <Image source={require('@assets/images/map/MinusButton.png')} />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={async () => {
              const camera = await mapView.current?.getCamera();
              Alert.alert(
                'Current camera',
                JSON.stringify(camera),
                [{text: 'OK'}],
                {
                  cancelable: true,
                },
              );
            }}
            style={[styles.controlItem]}>
            <Image source={require('@assets/images/map/PlusButton.png')} />
          </TouchableOpacity>
        </View>
        <View
          style={[
            styles.top,
            {
              top: insets.top,
            },
          ]}>
          <AppBackButton />
        </View>

        <MapView
          onRegionChangeComplete={onRegionChangeComplete}
          ref={mapView}
          provider={PROVIDER_GOOGLE}
          initialRegion={{
            latitude: 55.7558,
            longitude: 37.6173,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.034,
          }}
          mapPadding={{
            bottom: 230,
            left: 0,
            right: 0,
            top: 0,
          }}
          style={styles.map}>
          {addresses.map((item) => {
            const location = item.geometry
              ?.location as any as google.maps.LatLngLiteral;
            return (
              <Marker
                key={item.place_id as string}
                coordinate={{
                  latitude: location.lat,
                  longitude: location.lng,
                }}
                onPress={() => onSelectAddress(item)}>
                <Image
                  source={
                    item.place_id === selectedPlace?.place_id
                      ? MARKER_ICONS.SELECTED_LOCATION
                      : MARKER_ICONS.LOCATION
                  }
                />
              </Marker>
            );
          })}
        </MapView>
      </View>
      <View
        style={[
          styles.searchList,
          {
            bottom: SHEET_HEIGHT + insets.bottom + Spacing.SCALE_20,
          },
        ]}>
        <AddressSearchList
          onPress={(shop) => onSelectQuery(shop.name)}
          shops={shopsDummy}
        />
      </View>

      <AppBottomSheet
        title="На карте по названию"
        ref={sheet}
        enablePanDownToClose={false}
        snapPoints={snapPoints}
        enableOverDrag={false}
        keyboardBehavior="interactive"
        keyboardBlurBehavior="restore">
        <View style={[styles.sheetSearch]}>
          <View style={styles.input}>
            <TextFieldSheet
              value={query}
              onChangeText={setQuery}
              rightComponent={
                <TouchableOpacity
                  onPress={onNearbySearch}
                  style={styles.search}>
                  <Image
                    source={require('@assets/images/map/Search_white_icon_23.png')}
                  />
                </TouchableOpacity>
              }
            />
          </View>
          <AddressCardList
            ref={flatList}
            addresses={addresses}
            onPress={onSelectAddress}
            selectedPlace={selectedPlace}
          />
        </View>
      </AppBottomSheet>
    </SafeAreaView>
  );
};

const CONTROL_SIZE = 40;
const LOCATION_SIZE = 50;
const SEARCH_BUTTON_SIZE = 53;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    flex: 1,
    justifyContent: 'center',
  },
  input: {
    paddingHorizontal: Spacing.SCALE_16,
  },
  top: {
    marginHorizontal: Spacing.SCALE_16,
    marginTop: Spacing.SCALE_10,
    zIndex: 1,
    position: 'absolute',
    width: ScreenDimensions.width - Spacing.SCALE_16 * 2,
  },
  address: {
    marginTop: Spacing.SCALE_10,
    backgroundColor: 'rgba(255,255,255, 0.79)',
    padding: Spacing.SCALE_10,
    borderRadius: Borders.RADIUS_12,
  },
  addressText: {
    color: AppColors.BLACK,
    textAlign: 'center',
  },
  controls: {
    zIndex: 1,
    position: 'absolute',
    right: Spacing.SCALE_16,
    alignSelf: 'center',
  },
  controlItem: {
    width: CONTROL_SIZE,
    height: CONTROL_SIZE,
  },
  location: {
    height: LOCATION_SIZE,
    width: LOCATION_SIZE,
  },
  controlItemSpace: {
    marginBottom: Spacing.SCALE_15,
  },
  map: {
    ...StyleSheet.absoluteFillObject,
    width: '100%',
    height: ScreenDimensions.height,
  },
  sheetSearch: {
    height: '100%',
    backgroundColor: AppColors.WHITE_GRAY,
  },
  searchList: {
    position: 'absolute',
    left: 0,
    width: '100%',
  },
  search: {
    backgroundColor: AppColors.GRAY_PLACEHOLDER,
    height: '100%',
    width: SEARCH_BUTTON_SIZE,
    borderTopEndRadius: Borders.RADIUS_12,
    borderTopStartRadius: 3,
    borderBottomEndRadius: Borders.RADIUS_12,
    borderBottomStartRadius: 3,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
