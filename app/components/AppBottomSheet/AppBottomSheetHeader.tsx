import {AppColors, Borders} from '@styles/';
import commonStyles from '@styles/commonStyles';
import * as React from 'react';
import {StyleSheet, View, Text} from 'react-native';

interface Props {
  title?: string;
  onBack?(): void;
}
export const AppBottomSheetHeader: React.FC<Props> = ({title}) => {
  return (
    <View style={styles.container}>
      <View style={styles.block} />
      {title ? (
        <Text
          style={[
            styles.title,
            commonStyles.boldText16,
            commonStyles.textShadowBlack16,
          ]}>
          {title}
        </Text>
      ) : null}
    </View>
  );
};

const BLOCK_WIDTH = 61;
export const APP_BOTTOM_SHEET_HEADER_CONTAINER_HEIGHT = 38;

const styles = StyleSheet.create({
  container: {
    borderTopLeftRadius: Borders.RADIUS_22,
    borderTopRightRadius: Borders.RADIUS_22,
    alignItems: 'center',
    justifyContent: 'center',
    height: APP_BOTTOM_SHEET_HEADER_CONTAINER_HEIGHT,
    backgroundColor: AppColors.WHITE_GRAY,
  },
  block: {
    backgroundColor: AppColors.BLACK_MEDIUM,
    width: BLOCK_WIDTH,
    top: 0,
    position: 'absolute',
    height: 4,
    borderBottomLeftRadius: 4,
    borderBottomRightRadius: 4,
  },
  title: {
    color: AppColors.BLACK,
    textAlign: 'center',
  },
});
