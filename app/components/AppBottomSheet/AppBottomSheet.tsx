import * as React from 'react';
import {View} from 'react-native';
import BottomSheet, {BottomSheetProps} from '@gorhom/bottom-sheet';
import {AppBottomSheetHeader} from './AppBottomSheetHeader';

interface Props extends BottomSheetProps {
  title?: string;
  onBack?(): void;
}

export const AppBottomSheet = React.forwardRef<BottomSheet, Props>(
  ({onBack, title, ...props}, ref) => {
    return (
      <BottomSheet
        ref={ref}
        handleComponent={() => (
          <AppBottomSheetHeader title={title} onBack={onBack} />
        )}
        backgroundComponent={() => <View />}
        {...props}
      />
    );
  },
);
