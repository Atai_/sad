import React from 'react';
import {
  TouchableOpacity,
  StyleProp,
  ViewStyle,
  StyleSheet,
  Text,
} from 'react-native';
import {AppColors, Spacing, CommonStyles, Borders} from '@styles/';
import {scaleSize} from '@styles/mixins';
import LinearGradient from 'react-native-linear-gradient';

type AppButton = {
  leftText: string;
  rightText: string;
  onPress?(): void;
  disabled?: boolean;
  containerStyle?: StyleProp<ViewStyle>;
  withoutDisable?: boolean;
};

export const AppButton: React.FC<AppButton> = ({
  leftText,
  rightText,
  disabled = false,
  containerStyle,
  onPress,
}) => {
  return (
    <TouchableOpacity
      disabled={disabled}
      onPress={onPress}
      style={[
        styles.container,
        disabled && styles.disabledContainer,
        containerStyle,
      ]}>
      <LinearGradient
        colors={[AppColors.BLUE, AppColors.SKYBLUE]}
        useAngle
        angle={153}
        style={[styles.gradient, containerStyle]}>
        <Text
          style={[
            CommonStyles.boldText16,
            styles.text,
            disabled && styles.disabledText,
          ]}>
          {leftText}
        </Text>
        <Text
          style={[
            CommonStyles.boldText16,
            styles.text,
            disabled && styles.disabledText,
          ]}>
          от {rightText} ₽
        </Text>
      </LinearGradient>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: AppColors.WHITE,
    height: scaleSize(50),
    paddingHorizontal: Spacing.SCALE_20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: Borders.RADIUS_14,
  },
  text: {
    color: AppColors.WHITE,
  },
  gradient: {
    width: '100%',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  // Disabled styles
  disabledContainer: {
    backgroundColor: AppColors.GRAY_INPUT,
  },
  disabledIcon: {
    tintColor: AppColors.GRAY_DISABLED_TEXT,
  },
  disabledText: {
    color: AppColors.GRAY_DISABLED_TEXT,
  },
});
