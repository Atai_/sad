import {AppColors, Borders, Spacing, Typography} from '@styles/';
import commonStyles from '@styles/commonStyles';
import {
  FONT_FAMILY_UBUNTU_MEDIUM,
  FONT_SIZE_10,
  FONT_SIZE_14,
} from '@styles/typography';
import * as React from 'react';

import {
  View,
  Image,
  Pressable,
  StyleSheet,
  Text,
  ImageSourcePropType,
  Animated,
  TextStyle,
  StyleProp,
  ViewStyle,
  TouchableOpacity,
} from 'react-native';

interface Props {
  icon?: ImageSourcePropType;
  iconTextBottom?: string;
  iconTextBottomStyle?: StyleProp<TextStyle>;
  placeholder?: string;
  value?: string;
  onPress?(): void;
  containerStyle?: StyleProp<ViewStyle>;
  reverse?: boolean;
  iconRight?: ImageSourcePropType;
  iconRightOnPress?(): void;
}

export const ButtonText: React.FC<Props> = ({
  placeholder,
  value,
  icon,
  reverse,
  onPress,
  containerStyle,
  iconTextBottom,
  iconTextBottomStyle,
  iconRight,
  iconRightOnPress,
}) => {
  const textAnimate = React.useRef(new Animated.Value(0)).current;

  React.useEffect(() => {
    if (!reverse) {
      Animated.timing(textAnimate, {
        toValue: value ? 1 : 0,
        duration: 200,
        useNativeDriver: false,
      }).start();
    }
  }, [reverse, textAnimate, value]);
  const textStyle = {
    fontSize: textAnimate.interpolate({
      inputRange: [0, 1],
      outputRange: [FONT_SIZE_14, FONT_SIZE_10],
      extrapolate: 'clamp',
    }),
    marginBottom: textAnimate.interpolate({
      inputRange: [0, 1],
      outputRange: [2, 0],
      extrapolate: 'clamp',
    }),
  } as Animated.AnimatedProps<TextStyle>;
  const textContainerStyle = {
    flexDirection: reverse ? 'column-reverse' : 'column',
  } as ViewStyle;
  return (
    <View style={[styles.container, containerStyle]}>
      {icon ? (
        <View style={[styles.iconContainer]}>
          <Image source={icon} style={styles.icon} />
          {iconTextBottom ? (
            <Text
              style={[
                styles.iconTextBottom,
                commonStyles.boldText12,
                iconTextBottomStyle,
              ]}>
              {iconTextBottom}
            </Text>
          ) : null}
        </View>
      ) : null}
      <Pressable
        onPress={onPress}
        style={[styles.textContainer, textContainerStyle]}>
        <Animated.Text style={[styles.placeholder, textStyle]}>
          {placeholder}
        </Animated.Text>
        {value ? (
          <Text
            style={[
              styles.value,
              Typography.BOLD_14,
              Typography.TEXT_SHADOW_003,
            ]}>
            {value}
          </Text>
        ) : null}
        {iconRight ? (
          <TouchableOpacity
            onPress={iconRightOnPress}
            style={styles.iconRightContainer}>
            <Image source={iconRight} />
          </TouchableOpacity>
        ) : null}
      </Pressable>
    </View>
  );
};

const ICON_WIDTH = 50;
const MIN_CONTAINER_HEIGHT = 50;
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    minHeight: MIN_CONTAINER_HEIGHT,
  },
  iconContainer: {
    backgroundColor: AppColors.WHITE,
    alignItems: 'center',
    justifyContent: 'center',
    width: ICON_WIDTH,
    marginRight: Spacing.SCALE_5,
    borderRadius: Borders.RADIUS_10,
  },
  iconTextBottom: {
    color: AppColors.SKYBLUE,
    position: 'absolute',
    right: Spacing.SCALE_6,
    bottom: Spacing.SCALE_6,
  },
  icon: {},
  textContainer: {
    flex: 1,
    backgroundColor: AppColors.WHITE,
    justifyContent: 'center',
    paddingHorizontal: Spacing.SCALE_10,
    borderRadius: Borders.RADIUS_10,
  },
  placeholder: {
    color: AppColors.GRAY_TEXT,
    fontFamily: FONT_FAMILY_UBUNTU_MEDIUM,
  },
  value: {
    color: AppColors.BLACK_MEDIUM,
  },
  iconRightContainer: {
    position: 'absolute',
    right: Spacing.SCALE_10,
  },
});
