import React from 'react';
import {View, StyleSheet, Platform, Keyboard} from 'react-native';
import {BottomTabBarProps} from '@react-navigation/bottom-tabs';
import {TabButton} from './TabButton';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {TabIcon} from './TabIcon';
import {AppColors} from '@styles/';

export const AppTabBar: React.FC<BottomTabBarProps> = ({
  state,
  descriptors,
  navigation,
}) => {
  const [hide, setHide] = React.useState<boolean>(false);
  const insets = useSafeAreaInsets();

  const focusedOptions = descriptors[state.routes[state.index].key].options;
  React.useEffect(() => {
    Keyboard.addListener('keyboardDidShow', _keyboardDidShow);
    Keyboard.addListener('keyboardDidHide', _keyboardDidHide);
    return () => {
      Keyboard.removeAllListeners('keyboardDidShow');
      Keyboard.removeAllListeners('keyboardDidHide');
    };
  }, []);

  const _keyboardDidShow = () => {
    setHide(true);
  };

  const _keyboardDidHide = () => {
    setHide(false);
  };

  if (hide) {
    return null;
  }
  if (focusedOptions.tabBarVisible === false) {
    return null;
  }

  return (
    <View
      style={[
        styles.container,
        {
          paddingBottom: insets.bottom,
        },
      ]}>
      {state.routes.map((route, index) => {
        const {options} = descriptors[route.key];
        const label = options.title !== undefined ? options.title : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
          <TabButton
            key={route.name}
            icon={<TabIcon routeName={route.name} isFocused={isFocused} />}
            label={label}
            isFocused={isFocused}
            onPress={onPress}
            onLongPress={onLongPress}
          />
        );
      })}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'row',
    backgroundColor: AppColors.WHITE_GRAY,
    alignItems: 'flex-end',
    ...Platform.select({
      ios: {
        shadowOpacity: 0.4,
        shadowColor: AppColors.GRAY_MEDIUM,
        shadowRadius: 4,
        shadowOffset: {
          width: 0,
          height: 0,
        },
      },
      android: {
        elevation: 3,
      },
    }),
  },
});
