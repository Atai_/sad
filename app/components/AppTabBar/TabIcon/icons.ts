export const TabIcons = {
  MESSAGE: require('@assets/images/tabs/message.png'),
  MESSAGE_ACTIVE: require('@assets/images/tabs/message_active.png'),

  HOME: require('@assets/images/tabs/main.png'),
  HOME_ACTIVE: require('@assets/images/tabs/main_active.png'),

  ORDERS: require('@assets/images/tabs/orders.png'),
  ORDERS_ACTIVE: require('@assets/images/tabs/orders_active.png'),

  PROFILE: require('@assets/images/tabs/profile.png'),
  PROFILE_ACTIVE: require('@assets/images/tabs/profile_active.png'),
};
