import React from 'react';
import {Image, StyleSheet} from 'react-native';
import {NAVIGATION_BOTTOM_MENU} from '@navigation/screenNames';
import {TabIcons} from './icons';
import {scaleSize} from '@styles/mixins';

interface Props {
  routeName: string;
  isFocused: boolean;
}

export const TabIcon: React.FC<Props> = ({routeName, isFocused}) => {
  let icon = null;

  switch (routeName) {
    case NAVIGATION_BOTTOM_MENU.MESSAGES: {
      icon = isFocused ? TabIcons.MESSAGE_ACTIVE : TabIcons.MESSAGE;
      break;
    }

    case NAVIGATION_BOTTOM_MENU.HOME: {
      icon = isFocused ? TabIcons.HOME_ACTIVE : TabIcons.HOME;
      break;
    }

    case NAVIGATION_BOTTOM_MENU.ORDERS: {
      icon = isFocused ? TabIcons.ORDERS_ACTIVE : TabIcons.ORDERS;
      break;
    }

    case NAVIGATION_BOTTOM_MENU.PROFILE: {
      icon = isFocused ? TabIcons.PROFILE_ACTIVE : TabIcons.PROFILE;
      break;
    }

    default: {
      return null;
    }
  }
  return <Image source={icon} style={styles.icon} />;
};

const styles = StyleSheet.create({
  icon: {
    width: scaleSize(24),
    height: scaleSize(24),
    resizeMode: 'contain',
  },
});
