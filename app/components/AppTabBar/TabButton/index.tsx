import React from 'react';

import {Pressable, Text, StyleSheet, Animated, ViewStyle} from 'react-native';
import {AppColors, CommonStyles} from '@styles/';

interface Props {
  label: string;
  onPress(): void;
  onLongPress(): void;
  isFocused: boolean;
  /**
   * TODO: normal type
   */
  icon: any;
}

export const TabButton: React.FC<Props> = (props) => {
  const [pressed, setPressed] = React.useState(false);
  const pressedAnimated = React.useRef(new Animated.Value(1)).current;

  React.useEffect(() => {
    Animated.timing(pressedAnimated, {
      toValue: pressed ? 0.7 : 1,
      duration: 150,
      useNativeDriver: true,
    }).start();
  }, [pressed, pressedAnimated]);
  const style = {
    transform: [
      {
        scale: pressedAnimated,
      },
    ],
  } as any as ViewStyle;
  return (
    <Pressable
      style={styles.container}
      onPress={props.onPress}
      onPressIn={() => setPressed(true)}
      onPressOut={() => setPressed(false)}
      onLongPress={props.onLongPress}>
      <Animated.View style={[styles.buttonContainer, style]}>
        {props.icon}
        <Text
          style={[
            CommonStyles.mediumText10,
            styles.text,
            props.isFocused && styles.focusedText,
          ]}>
          {props.label}
        </Text>
      </Animated.View>
    </Pressable>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonContainer: {
    alignItems: 'center',
  },
  text: {
    color: AppColors.GRAY,
    marginTop: 3,
  },
  focusedText: {
    color: AppColors.SKYBLUE,
  },
});
