import * as React from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  Image,
  ImageSourcePropType,
} from 'react-native';

import {AppColors, Borders, Spacing} from '@styles/';
import commonStyles from '@styles/commonStyles';

interface Props {
  onPress?(): void;
  title: string;
  icon: ImageSourcePropType;
}

export const ButtonIcon: React.FC<Props> = ({onPress, title, icon}) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles.container}>
      {icon ? <Image style={styles.icon} source={icon} /> : null}
      <Text style={[styles.text, commonStyles.mediumText10]}>{title}</Text>
    </TouchableOpacity>
  );
};

const BUTTON_HEIGHT = 50;
const styles = StyleSheet.create({
  container: {
    height: BUTTON_HEIGHT,
    borderRadius: Borders.RADIUS_12,
    backgroundColor: AppColors.WHITE,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    paddingHorizontal: Spacing.SCALE_10,
  },

  text: {
    color: AppColors.GRAY_TEXT,
  },
  icon: {
    marginRight: Spacing.SCALE_2,
  },
});
