import * as React from 'react';
import {StyleSheet, Text, View, FlatList, ViewToken} from 'react-native';
import Modal from 'react-native-modal';
import {AppColors, Borders, Spacing} from '@styles/';
import commonStyles from '@styles/commonStyles';
import {AppBottomSheetHeader} from '@components/AppBottomSheet/AppBottomSheetHeader';

type DataItem = {
  title?: string;
  value: any;
};
interface Props {
  title?: string;
  description?: string;
  visible: boolean;
  onClose?(): void;
  data?: DataItem[];
  defaultValue?: any;
}

export const AppModalSelect: React.FC<Props> = ({
  title,
  description,
  visible,
  onClose,
  data = [],
  defaultValue,
}) => {
  const flatList = React.useRef<FlatList>(null);
  const viewabilityConfig = React.useRef({
    itemVisiblePercentThreshold: 50,
  });
  const onViewableItemsChanged = React.useRef(
    (info: {viewableItems: Array<ViewToken>; changed: Array<ViewToken>}) => {
      setSelectedValue(info.viewableItems![1].item.value);
    },
  );

  const [selectedValue, setSelectedValue] = React.useState<any>(null);

  const goToItem = (index: number) => {
    flatList.current?.scrollToIndex({
      animated: true,
      index,
    });
  };

  const onShowModal = () => {
    const findIndex = data.findIndex((item) => item.value === defaultValue);

    if (findIndex) {
      goToItem(findIndex);
    }
  };

  return (
    <Modal
      onShow={onShowModal}
      onSwipeComplete={() => onClose && onClose()}
      swipeDirection="down"
      isVisible={visible}
      style={styles.modal}
      propagateSwipe>
      <View style={styles.container}>
        <View style={styles.info}>
          <Text style={[styles.title, commonStyles.boldText26]}>{title}</Text>
          <Text style={[styles.description, commonStyles.mediumText]}>
            {description}
          </Text>
        </View>
        <View style={styles.content}>
          <AppBottomSheetHeader />
          <View
            style={styles.scrollContainer}
            onStartShouldSetResponder={() => true}>
            <FlatList
              ref={flatList}
              scrollEventThrottle={16}
              data={[
                {
                  title: '',
                  value: '',
                },
                ...data,
                {
                  title: '',
                  value: '',
                },
              ]}
              bounces={false}
              snapToAlignment={'center'}
              snapToInterval={50}
              viewabilityConfig={viewabilityConfig.current}
              onViewableItemsChanged={onViewableItemsChanged.current}
              keyExtractor={(_, index) => index.toString()}
              renderItem={({item}) => {
                const isActive = selectedValue === item.value;

                return (
                  <View
                    onStartShouldSetResponder={() => true}
                    style={[styles.item, isActive ? styles.itemActive : null]}>
                    <Text
                      style={[
                        styles.titleItem,
                        isActive ? styles.titleItemActive : null,
                        commonStyles.boldText16,
                      ]}>
                      {item?.title}
                    </Text>
                  </View>
                );
              }}
              pagingEnabled
              contentContainerStyle={styles.scrollContent}
            />
          </View>
        </View>
      </View>
    </Modal>
  );
};
const CONTENT_HEIGHT = 300;
const styles = StyleSheet.create({
  modal: {
    flexDirection: 'column',
    justifyContent: 'flex-end',
    margin: 0,
    flex: 1,
  },
  container: {},
  title: {
    color: AppColors.WHITE,
  },
  description: {
    color: AppColors.WHITE,
  },
  content: {
    borderTopLeftRadius: Borders.RADIUS_22,
    borderTopRightRadius: Borders.RADIUS_22,
    backgroundColor: AppColors.WHITE_GRAY,
    height: CONTENT_HEIGHT,
  },
  info: {
    marginHorizontal: Spacing.SCALE_16,
    marginBottom: Spacing.SCALE_14,
  },
  item: {
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    opacity: 0.4,
  },
  itemActive: {
    opacity: 1,
    backgroundColor: AppColors.WHITE,
  },

  scrollContainer: {
    height: 150,
    paddingHorizontal: Spacing.SCALE_16,
  },
  scrollContent: {
    flexGrow: 1,
  },

  titleItem: {
    color: AppColors.BLACK,
  },
  titleItemActive: {
    color: AppColors.BLACK,
  },
});
