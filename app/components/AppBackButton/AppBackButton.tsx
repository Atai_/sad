import * as React from 'react';
import {TouchableOpacity, Image, StyleSheet} from 'react-native';
import {useNavigation} from '@react-navigation/core';
import {AppColors, Borders} from '@styles/';

interface Props {
  onPress?(): void;
}
export const AppBackButton: React.FC<Props> = ({onPress}) => {
  const navigation = useNavigation();
  const goBack = onPress || navigation.goBack;
  return (
    <TouchableOpacity style={styles.container} onPress={goBack}>
      <Image source={require('@assets/images/icons/Back_Grey_Icon_15.png')} />
    </TouchableOpacity>
  );
};

const CONTAINER_SIZE = 40;

const styles = StyleSheet.create({
  container: {
    backgroundColor: AppColors.WHITE,
    width: CONTAINER_SIZE,
    height: CONTAINER_SIZE,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: Borders.RADIUS_10,
    elevation: 2,
    shadowColor: AppColors.BLACK,
    shadowOpacity: 0.15,
    shadowRadius: 3,
    shadowOffset: {
      width: 0,
      height: 0,
    },
  },
});
