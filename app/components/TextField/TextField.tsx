import * as React from 'react';
import {
  Platform,
  StyleProp,
  StyleSheet,
  TextInput,
  View,
  TextInputProps,
  ViewStyle,
} from 'react-native';
import {AppColors, Borders, Spacing} from '@styles/';
import commonStyles from '@styles/commonStyles';

export interface TextFieldProps extends TextInputProps {
  containerStyle?: StyleProp<ViewStyle>;
  rightComponent?: React.ReactNode;
}

export const TextField: React.FC<TextFieldProps> = ({
  containerStyle,
  rightComponent,
  ...props
}) => {
  return (
    <View style={[styles.container, containerStyle]}>
      <TextInput
        style={[
          styles.input,
          props.value ? commonStyles.boldText14 : commonStyles.mediumText,
          props.value ? commonStyles.textShadowBlack16 : null,
        ]}
        placeholderTextColor={AppColors.GRAY_PLACEHOLDER}
        {...props}
      />
      {rightComponent ? (
        <View style={styles.left}>{rightComponent}</View>
      ) : null}
    </View>
  );
};

const INPUT_SIZE = 40;

const styles = StyleSheet.create({
  container: {
    borderRadius: Borders.RADIUS_13,
    backgroundColor: AppColors.WHITE,
    height: INPUT_SIZE,
    ...Platform.select({
      ios: {
        shadowOffset: {
          height: 0,
          width: 0,
        },
        shadowColor: '#00000029',
        shadowRadius: 6,
      },
      android: {
        elevation: 2,
      },
    }),
  },
  input: {
    padding: Spacing.SCALE_12,
    color: AppColors.BLACK,
  },
  left: {
    position: 'absolute',
    right: 0,
    height: '100%',
  },
});
