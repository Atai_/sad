import {useBottomSheetInternal} from '@gorhom/bottom-sheet';
import React, {useCallback} from 'react';
import {TextField} from '.';
import {TextFieldProps} from './TextField';

export const TextFieldSheet: React.FC<TextFieldProps> = (props) => {
  const {shouldHandleKeyboardEvents} = useBottomSheetInternal();
  //#endregion

  //#region callbacks
  const handleOnFocus = useCallback(() => {
    shouldHandleKeyboardEvents.value = true;
  }, [shouldHandleKeyboardEvents]);
  const handleOnBlur = useCallback(() => {
    shouldHandleKeyboardEvents.value = false;
  }, [shouldHandleKeyboardEvents]);
  //#endregion

  return <TextField onFocus={handleOnFocus} onBlur={handleOnBlur} {...props} />;
};
