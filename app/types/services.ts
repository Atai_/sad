type ServicePlace = {
  amount?: number;
  address: {
    id?: number;
    street: string;
    number: string;
  } | null;
};

export type ShoppingCourierPlaces = ServicePlace[];
