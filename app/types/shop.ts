export type ShopDummy = {
  id: number;
  name: string;
  duration: number;
  description: string;
  icon_url: string;
};
