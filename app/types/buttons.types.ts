import {StyleProp, ViewStyle} from 'react-native';

export type ButtonStyle = 'secondary' | 'primary' | 'outline';

export type TButton = {
  title: string;
  onPress(): void;
  type?: ButtonStyle;
  style?: StyleProp<ViewStyle>;
};
