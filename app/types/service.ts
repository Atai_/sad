export enum ServiceTypes {
  SHOPPING_COURIER = 'shopping_courier',
  DELIVERY_COURIER = 'delivery_courier',
  ORDER_COURIER = 'order_courier',
}

export type ServiceTypesLiteral = `${ServiceTypes}`;
