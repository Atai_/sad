export const RADIUS_22 = 22;
export const RADIUS_20 = 20;
export const RADIUS_16 = 16;
export const RADIUS_15 = 15;
export const RADIUS_14 = 14;
export const RADIUS_13 = 13;
export const RADIUS_12 = 12;
export const RADIUS_11 = 11;
export const RADIUS_10 = 10;
export const RADIUS_9 = 9;
export const RADIUS_8 = 8;

export const BORDER_COLOR_GRAY = '#EFEFEF';
