import {TextStyle} from 'react-native';
export const FONT_SIZE_40 = 40;
export const FONT_SIZE_36 = 36;
export const FONT_SIZE_34 = 34;
export const FONT_SIZE_30 = 30;
export const FONT_SIZE_26 = 26;
export const FONT_SIZE_24 = 24;
export const FONT_SIZE_22 = 22;
export const FONT_SIZE_20 = 20;
export const FONT_SIZE_18 = 18;
export const FONT_SIZE_16 = 16;
export const FONT_SIZE_14 = 14;
export const FONT_SIZE_12 = 12;
export const FONT_SIZE_10 = 10;

export const LINE_HEIGHT_40 = 40;
export const LINE_HEIGHT_36 = 36;
export const LINE_HEIGHT_34 = 34;
export const LINE_HEIGHT_30 = 30;
export const LINE_HEIGHT_28 = 28;
export const LINE_HEIGHT_35 = 26;
export const LINE_HEIGHT_24 = 24;
export const LINE_HEIGHT_22 = 22;
export const LINE_HEIGHT_20 = 20;
export const LINE_HEIGHT_18 = 18;
export const LINE_HEIGHT_16 = 16;
export const LINE_HEIGHT_14 = 14;
export const LINE_HEIGHT_13 = 13;

export const FONT_FAMILY_UBUNTU_BOLD = 'Ubuntu-Bold';
export const FONT_FAMILY_UBUNTU_MEDIUM = 'Ubuntu-Medium';
export const FONT_FAMILY_UBUNTU_REGULAR = 'Ubuntu-Regular';
export const FONT_FAMILY_UBUNTU_LIGHT = 'Ubuntu-Light';

export const MEDIUM_10 = {
  fontFamily: FONT_FAMILY_UBUNTU_MEDIUM,
  fontSize: FONT_SIZE_10,
} as TextStyle;

export const BOLD_14 = {
  fontFamily: FONT_FAMILY_UBUNTU_BOLD,
  fontSize: FONT_SIZE_14,
} as TextStyle;

export const TEXT_SHADOW_003 = {
  textShadowColor: '#00000029',
  textShadowOffset: {
    width: 0,
    height: 0,
  },
  textShadowRadius: 6,
} as TextStyle;
