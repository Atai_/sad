export const SKYBLUE = '#00D8F9';
export const BLUE = '#00B0F0';
export const BLUE_LIGHT = '#EBFCFF';
export const GRAY = '#ADADAD';
export const GRAY_LIGHT = '#AAAAAA';
export const GRAY_MEDIUM = '#C4C4C4';
export const GRAY_DISABLED_TEXT = '#C6C6C6';
export const GRAY_INPUT = '#EFEFEF';

export const BG_WHITE = '#FDFDFD';

export const BLACK = '#2E3F41';
export const BLACK_MEDIUM = '#4B595C';
export const BLACK_LIGHT = '#7E7E7E';

export const WHITE = '#fff';
export const WHITE_GRAY = '#F8F8F8';

export const RED = '#FA4B4B';

export const RED_GRADIENT = '#E02F2F';

export const GRAY_TEXT = '#B2BDC5';
export const GRAY_PLACEHOLDER = '#DEDEDE';

export const VIOLET = '#C955AE';
export const VIOLET_LIGHT = '#C056E0';
export const VIOLET_TEXT = '#C255D4';
export const GRAY_SHADOW = '#00000029';
