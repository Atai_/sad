import {} from './mixins';

export const SCALE_50 = 50;

export const SCALE_45 = 45;
export const SCALE_42 = 42;
export const SCALE_40 = 40;
export const SCALE_38 = 38;
export const SCALE_35 = 35;
export const SCALE_32 = 32;
export const SCALE_30 = 30;
export const SCALE_25 = 25;
export const SCALE_22 = 22;
export const SCALE_20 = 20;
export const SCALE_16 = 16;
export const SCALE_15 = 15;
export const SCALE_14 = 14;
export const SCALE_12 = 12;
export const SCALE_10 = 10;
export const SCALE_8 = 8;
export const SCALE_7 = 7;
export const SCALE_6 = 6;
export const SCALE_5 = 5;
export const SCALE_4 = 4;
export const SCALE_2 = 4;
