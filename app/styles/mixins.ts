import {Dimensions, PixelRatio, Platform} from 'react-native';

const WINDOW_WIDTH = Dimensions.get('window').width;
const guidelineBaseWidth = 375;
type ShadowArg = {
  color: string;
  radius: number;
  width?: number;
  height?: number;
  opacity?: number;
};
export const scaleSize = (size: number) =>
  (WINDOW_WIDTH / guidelineBaseWidth) * size;

export const scaleFont = (size: number) => size * PixelRatio.getFontScale();

export function boxShadow({
  color,
  radius,
  width = 0,
  height = 0,
  opacity = 1,
}: ShadowArg) {
  return Platform.select({
    ios: {
      shadowRadius: opacity,
      shadowOffset: {
        width,
        height,
      },
      shadowColor: color,
      shadowOpacity: opacity,
    },
    android: {
      elevation: (radius || 1) * 0.5,
    },
  });
}
