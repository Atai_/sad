import {StyleSheet} from 'react-native';
import {Typography, AppColors} from './index';

const commonStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: AppColors.WHITE_GRAY,
  },
  containerWhite: {
    flex: 1,
    backgroundColor: AppColors.WHITE,
  },
  containerWithHeader: {
    flex: 1,
  },
  headingText: {
    fontSize: Typography.FONT_SIZE_24,
    fontFamily: Typography.FONT_FAMILY_UBUNTU_BOLD,
  },
  headingText34: {
    fontSize: Typography.FONT_SIZE_34,
    fontFamily: Typography.FONT_FAMILY_UBUNTU_BOLD,
  },
  titleText: {
    fontSize: Typography.FONT_SIZE_20,
    fontFamily: Typography.FONT_FAMILY_UBUNTU_REGULAR,
  },
  /**
   * Bold text start
   */
  boldText10: {
    fontSize: Typography.FONT_SIZE_10,
    fontFamily: Typography.FONT_FAMILY_UBUNTU_BOLD,
  },
  boldText12: {
    fontSize: Typography.FONT_SIZE_12,
    fontFamily: Typography.FONT_FAMILY_UBUNTU_BOLD,
  },
  boldText14: {
    fontSize: Typography.FONT_SIZE_14,
    fontFamily: Typography.FONT_FAMILY_UBUNTU_BOLD,
  },
  boldText16: {
    fontSize: Typography.FONT_SIZE_16,
    fontFamily: Typography.FONT_FAMILY_UBUNTU_BOLD,
  },
  boldText18: {
    fontSize: Typography.FONT_SIZE_18,
    fontFamily: Typography.FONT_FAMILY_UBUNTU_BOLD,
  },
  boldText20: {
    fontSize: Typography.FONT_SIZE_20,
    fontFamily: Typography.FONT_FAMILY_UBUNTU_BOLD,
  },
  boldText24: {
    fontSize: Typography.FONT_SIZE_24,
    fontFamily: Typography.FONT_FAMILY_UBUNTU_BOLD,
    lineHeight: Typography.LINE_HEIGHT_28,
  },
  boldText26: {
    fontSize: Typography.FONT_SIZE_26,
    fontFamily: Typography.FONT_FAMILY_UBUNTU_BOLD,
    lineHeight: Typography.LINE_HEIGHT_28,
  },
  boldText30: {
    fontSize: Typography.FONT_SIZE_30,
    fontFamily: Typography.FONT_FAMILY_UBUNTU_BOLD,
  },
  boldText36: {
    fontSize: Typography.FONT_SIZE_36,
    fontFamily: Typography.FONT_FAMILY_UBUNTU_BOLD,
  },
  /**
   * Medium text start
   */
  mediumText: {
    fontSize: Typography.FONT_SIZE_12,
    fontFamily: Typography.FONT_FAMILY_UBUNTU_MEDIUM,
  },
  mediumText14: {
    fontSize: Typography.FONT_SIZE_14,
    fontFamily: Typography.FONT_FAMILY_UBUNTU_MEDIUM,
  },
  mediumText10: {
    fontSize: Typography.FONT_SIZE_10,
    fontFamily: Typography.FONT_FAMILY_UBUNTU_MEDIUM,
  },
  mediumText16: {
    fontSize: Typography.FONT_SIZE_16,
    fontFamily: Typography.FONT_FAMILY_UBUNTU_MEDIUM,
  },
  mediumText18: {
    fontSize: Typography.FONT_SIZE_18,
    fontFamily: Typography.FONT_FAMILY_UBUNTU_MEDIUM,
  },
  mediumText20: {
    fontSize: Typography.FONT_SIZE_20,
    fontFamily: Typography.FONT_FAMILY_UBUNTU_MEDIUM,
  },
  mediumText22: {
    fontSize: Typography.FONT_SIZE_22,
    fontFamily: Typography.FONT_FAMILY_UBUNTU_MEDIUM,
  },
  mediumText30: {
    fontSize: Typography.FONT_SIZE_30,
    fontFamily: Typography.FONT_FAMILY_UBUNTU_MEDIUM,
  },
  mediumText36: {
    fontSize: Typography.FONT_SIZE_36,
    fontFamily: Typography.FONT_FAMILY_UBUNTU_MEDIUM,
  },
  mediumText40: {
    fontSize: Typography.FONT_SIZE_40,
    fontFamily: Typography.FONT_FAMILY_UBUNTU_MEDIUM,
  },
  /**
   * Regular text start
   */
  regularText: {
    fontSize: Typography.FONT_SIZE_12,
    fontFamily: Typography.FONT_FAMILY_UBUNTU_REGULAR,
    lineHeight: Typography.LINE_HEIGHT_13,
  },
  regularText10: {
    fontSize: Typography.FONT_SIZE_10,
    fontFamily: Typography.FONT_FAMILY_UBUNTU_REGULAR,
    lineHeight: Typography.LINE_HEIGHT_13,
  },
  regularText14: {
    fontSize: Typography.FONT_SIZE_14,
    fontFamily: Typography.FONT_FAMILY_UBUNTU_REGULAR,
  },
  regularText16: {
    fontSize: Typography.FONT_SIZE_16,
    fontFamily: Typography.FONT_FAMILY_UBUNTU_REGULAR,
  },
  regularText20: {
    fontSize: Typography.FONT_SIZE_20,
    fontFamily: Typography.FONT_FAMILY_UBUNTU_REGULAR,
  },
  regularText24: {
    fontSize: Typography.FONT_SIZE_24,
    fontFamily: Typography.FONT_FAMILY_UBUNTU_REGULAR,
  },

  /**
   * text shadow
   */

  textShadowBlack16: {
    textShadowColor: 'rgba(0, 0, 0, 0.16)',
    textShadowOffset: {
      width: 0,
      height: 0,
    },
    textShadowRadius: 3,
  },
  errorText: {
    color: 'red',
  },
});

export default commonStyles;
