export const parseAddressComponents = (
  // eslint-disable-next-line no-undef
  components: google.maps.GeocoderAddressComponent[],
) => {
  const number =
    components?.find((item) => item.types.includes('street_number'))
      ?.long_name || '';
  const street =
    components?.find((item) => item.types.includes('route'))?.long_name || '';
  const country =
    components?.find((item) => item.types.includes('country'))?.long_name ||
    'Россия';
  const city =
    components?.find((item) => item.types.includes('locality'))?.long_name ||
    'Москва';

  return {
    street,
    city,
    number,
    country,
  };
};
