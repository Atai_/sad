import {ShopDummy} from '@customTypes/shop';
import {request} from './request';

export const ShopsApi = {
  getDummyList() {
    return request.get<ShopDummy[]>('/shops/shopdummy/');
  },
};
