import axios from 'axios';

const AxiosApi = axios.create({
  baseURL: 'https://admin.test-spotapp.ru/api',
});

export {AxiosApi as request};
