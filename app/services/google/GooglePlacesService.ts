import {GOOGLE_API_KEY} from '@config/index';
import axios from 'axios';

const test_KEY = __DEV__ ? `AIzaSyAFZamD2JF2uNNmI2wTMGi-CtPveXO-w1Y` : '';

type PlacesNearbyResponse = {
  results: google.maps.places.PlaceResult[];
  html_attributions: string[];
  status: google.maps.places.PlacesServiceStatus;
  error_message?: string;
  info_messages?: string;
  next_page_token?: string;
};
export type PlaceLocationParam = {lat: number; lng: number};
export const GooglePlacesService = {
  _DEFAULT_LANGUAGE: 'ru',
  getQueryPlaces(input: string) {
    const url =
      'https://maps.googleapis.com/maps/api/place/queryautocomplete/json';

    return axios.get<google.maps.places.AutocompleteResponse>(url, {
      params: {
        input,
        language: this._DEFAULT_LANGUAGE,
        key: test_KEY || GOOGLE_API_KEY,
      },
    });
  },
  locationGeocoding(location: PlaceLocationParam) {
    const url = 'https://maps.googleapis.com/maps/api/geocode/json';
    return axios.get<google.maps.GeocoderResponse>(url, {
      params: {
        location: `${location.lat},${location.lng}`,
        language: this._DEFAULT_LANGUAGE,
        key: test_KEY || GOOGLE_API_KEY,
      },
    });
  },
  nearbySearch(keyword: string, location: PlaceLocationParam) {
    const url = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json';
    return axios.get<PlacesNearbyResponse>(url, {
      params: {
        keyword,
        location: `${location.lat},${location.lng}`,
        language: this._DEFAULT_LANGUAGE,
        rankby: 'distance',
        key: test_KEY || GOOGLE_API_KEY,
      },
    });
  },
};
